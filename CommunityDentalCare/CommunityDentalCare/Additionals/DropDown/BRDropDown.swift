//
//  BRDropDown.swift
//  PeopleCenter
//
//  Created by Berlin Raj on 20/09/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit
protocol BRDropDownDelegate {
    func dropDown(dropDown: BRDropDown, selectedAtIndex index: Int, selectedOption option: String?)
}
class BRDropDown: MCView {
    
    @IBInspectable var dropDownRowHeight: CGFloat!
    @IBInspectable var dropDownBorderColor: UIColor = UIColor.blackColor() {
        willSet {
            
        } didSet {
            tableView?.layer.borderColor = dropDownBorderColor.CGColor
        }
    }
    var placeholder: String = "-- SELECT --" {
        willSet {
            if enablePlaceholder {
                self.labelTitle.text = newValue
                self.dropDownButton.tag = 0
            } else if items.count > 0 {
                self.dropDownButton.tag = 1
                self.labelTitle.text = items[0].uppercaseString
            }
        }
    }
    var delegate: BRDropDownDelegate?
    var items: [String]! {
        willSet {
            if enablePlaceholder {
                self.labelTitle.text = placeholder
                self.dropDownButton.tag = 0
            } else {
                self.dropDownButton.tag = 1
                self.labelTitle.text = newValue[0].uppercaseString
            }
        } didSet {
            self.tableView.reloadData()
        }
    }
    var selected: Bool = false {
        willSet {
            dropDownButton.selected = newValue
            if newValue == true {
                showDropDown()
            } else {
                hideDropDown()
            }
        } didSet {
            
        }
    }
    var dropDownOptionSelected: Bool {
        get {
            return dropDownButton.tag > 0
        }
    }
    var selectedOption: String? {
        get {
            return dropDownButton.tag == 0 ? nil : items[dropDownButton.tag - 1]
        }
    }
    func reset() {
        self.selectedIndex = 0
    }
    var selectedIndex: Int {
        get {
            return dropDownButton.tag
        } set {
            if newValue == 0 {
                self.labelTitle.text = enablePlaceholder ? placeholder : items[0].uppercaseString
                self.dropDownButton.tag = enablePlaceholder ? 0 : 1
            } else if newValue <= items.count {
                self.labelTitle.text = items[newValue - 1]
                self.dropDownButton.tag = newValue
            }
        }
    }
    
//    
//    var setSelectedIndex : Int = 0 {
//        willSet{
//            
//            
//            self.labelTitle.text = newValue
//            if newValue == "-- SELECT --" {
//                dropDownButton.tag = 0
//            }else{
//                self.dropDownButton.tag = items.indexOf(newValue)! + 1
//            }
//        }
//    }
    
    var setSelectedOption : String = "-- SELECT --"{
        willSet{
            self.labelTitle.text = newValue
            if newValue == "-- SELECT --" {
                dropDownButton.tag = 0
            }else{
                self.dropDownButton.tag = items.indexOf(newValue)! + 1
            }
        }
    }
    @IBInspectable var enablePlaceholder: Bool = true
    @IBInspectable var dropDownImage: UIImage? {
        willSet {
            
        } didSet {
            dropDownButton?.setBackgroundImage(dropDownImage, forState: UIControlState.Normal)
        }
    }
    @IBInspectable var dropDownSelectedImage: UIImage? {
        willSet {
            
        } didSet {
            dropDownButton?.setBackgroundImage(dropDownSelectedImage, forState: UIControlState.Selected)
        }
    }
    
    var dropDownButton: UIButton!
    var labelTitle: UILabel!
    var tableView: UITableView!
    var superViewFrame: CGRect!
    
    var tableRowHeight: CGFloat {
        get {
            if dropDownRowHeight == nil {
                return CGRectGetHeight(dropDownButton.frame)
            }
            return dropDownRowHeight
        }
    }
    @IBInspectable var selectedFont: UIFont = UIFont(name: "WorkSans-Regular", size: 17)! {
        willSet {
            
        } didSet {
            self.labelTitle?.font = selectedFont
        }
    }
    @IBInspectable var listFont: UIFont = UIFont(name: "WorkSans-Regular", size: 17)! {
        willSet {
            
        } didSet {
            self.tableView?.reloadData()
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initView()
    }
    func initView() {
        self.backgroundColor = UIColor.clearColor()
        labelTitle = UILabel()
        labelTitle.textColor = UIColor.whiteColor()
        labelTitle.textAlignment = NSTextAlignment.Center
        labelTitle.font = UIFont(name: "WorkSans-Regular", size: 17)
        labelTitle.adjustsFontSizeToFitWidth = true
        self.addSubview(labelTitle)
        
        dropDownButton = UIButton()
        dropDownButton.setBackgroundImage(dropDownImage, forState: UIControlState.Normal)
        dropDownButton.setBackgroundImage(dropDownSelectedImage, forState: UIControlState.Selected)
        dropDownButton.layer.borderColor = UIColor.whiteColor().CGColor
        dropDownButton.layer.borderWidth = 1.0
        dropDownButton.layer.cornerRadius = 3.0
        clipsToBounds = true
        dropDownButton.addTarget(self, action: #selector(BRDropDown.dropDownButtonAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        self.addSubview(dropDownButton)
        
        tableView = UITableView()
        tableView.backgroundColor = UIColor.clearColor()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = UITableViewCellSeparatorStyle.None
        tableView.separatorColor = UIColor.clearColor()
        tableView.layer.cornerRadius = 3.0
        tableView.layer.masksToBounds = true
        
        self.addSubview(tableView)
    }
    func showDropDown() {
        (self.superview!.subviews as NSArray).enumerateObjectsUsingBlock { (obj, idx, stop) in
            if let otherdropDown = obj as? BRDropDown {
                if otherdropDown != self {
                    otherdropDown.selected = false
                }
            }
        }
        self.superview?.bringSubviewToFront(self)
        UIView.animateWithDuration(0.3, animations: {
            var frame = self.frame
            frame.size.height = min(screenSize.height * (2/3), CGRectGetHeight(self.dropDownButton.frame) + self.tableRowHeight * CGFloat(self.items.count))
            self.frame = frame
            self.tableView.frame = CGRectMake(0, CGRectGetHeight(self.dropDownButton.frame), CGRectGetWidth(self.frame), CGRectGetHeight(self.frame) - CGRectGetHeight(self.dropDownButton.frame))
            }) { (finished) in
                let diff = CGRectGetMaxY(self.frame) - CGRectGetHeight(self.superViewFrame)
                if diff > 0 {
                    UIView.animateWithDuration(0.1, animations: {
                        self.superview?.frame.size.height = CGRectGetHeight(self.superViewFrame) + diff + 20
                        self.superview?.frame.origin.y = CGRectGetMinY(self.superViewFrame) - diff - 20
                    }) { (finished) in
                        if Int(CGRectGetHeight(self.tableView.frame)/self.tableRowHeight) < self.items.count {
                            UIView.animateWithDuration(0.2, animations: {
                                self.tableView.scrollToRowAtIndexPath(NSIndexPath(forRow: self.items.count - 1, inSection: 0), atScrollPosition: UITableViewScrollPosition.Bottom, animated: true)
                            })
                        }
                    }
                } else if Int(CGRectGetHeight(self.tableView.frame)/self.tableRowHeight) < self.items.count {
                    UIView.animateWithDuration(0.2, animations: {
                        self.tableView.scrollToRowAtIndexPath(NSIndexPath(forRow: self.items.count - 1, inSection: 0), atScrollPosition: UITableViewScrollPosition.Bottom, animated: true)
                    })
                }
        }
    }
    func hideDropDown() {
        UIView.animateWithDuration(0.3, animations: {
            var frame = self.frame
            frame.size.height = CGRectGetHeight(self.dropDownButton.frame)
            self.frame = frame
            self.tableView.frame = CGRectMake(0, CGRectGetHeight(self.dropDownButton.frame), CGRectGetWidth(self.frame), 0)
        }) { (finished) in
            UIView.animateWithDuration(0.2, animations: {
                self.superview?.frame = self.superViewFrame
            })
        }
    }
    
    func dropDownButtonAction (sender: UIButton) {
        self.selected = !self.selected
    }
    
    override func willMoveToSuperview(newSuperview: UIView?) {
        super.willMoveToSuperview(newSuperview)
        dropDownButton.frame = CGRectMake(0, 0, CGRectGetWidth(self.frame), CGRectGetHeight(self.frame))
        labelTitle.frame = CGRectMake(0, 0, CGRectGetWidth(self.frame) - CGRectGetHeight(self.dropDownButton.frame), CGRectGetHeight(self.frame))
        self.tableView.frame = CGRectMake(0, CGRectGetHeight(self.dropDownButton.frame), CGRectGetWidth(self.frame), 0)
        if let superView = newSuperview {
            self.superViewFrame = superView.frame
        }
    }
    
    func addItem(item: String) {
        if items == nil {
            items = [String]()
        }
        items.append(item)
    }
}
extension BRDropDown: UITableViewDelegate, UITableViewDataSource {
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return self.tableRowHeight
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return items != nil ? 1 : 0
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("Cell") as? BRDropDownCell
        if cell == nil {
            cell = BRDropDownCell(style: UITableViewCellStyle.Default, reuseIdentifier: "Cell")
            cell?.backgroundColor = UIColor.whiteColor()
            cell?.selectionStyle = UITableViewCellSelectionStyle.None
        }
        cell!.title = items[indexPath.row].uppercaseString
        cell!.titleColor = self.dropDownBorderColor
        cell!.titleFont = self.listFont
        
        return cell!
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.labelTitle.text = items[indexPath.row].uppercaseString
        self.dropDownButton.tag = indexPath.row + 1
        self.selected = false
        
        self.delegate?.dropDown(self, selectedAtIndex: selectedIndex, selectedOption: selectedOption)
    }
    
    class BRDropDownCell: UITableViewCell {
        
        var labelTitle: UILabel!
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            super.init(style: style, reuseIdentifier: reuseIdentifier)
            
            labelTitle = UILabel()
            labelTitle.textColor = titleColor
            labelTitle.numberOfLines = 0
            labelTitle.font = UIFont(name: "WorkSans-Regular", size: 17)
            labelTitle.textAlignment = NSTextAlignment.Center
            self.addSubview(labelTitle)
        }
        override func layoutSubviews() {
            super.layoutSubviews()
            labelTitle.frame = CGRectMake(0, 0, CGRectGetWidth(self.frame), CGRectGetHeight(self.frame))
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        var title: String! {
            willSet {
                self.labelTitle.text = newValue
            } didSet {
                
            }
        }
        var titleColor: UIColor = UIColor.blackColor() {
            willSet {
                self.labelTitle.textColor = newValue
            } didSet {
                
            }
        }
        var titleFont: UIFont = UIFont(name: "WorkSans-Regular", size: 17)! {
            willSet {
                
            } didSet {
                self.labelTitle.font = titleFont
            }
        }
    }
}
