//
//  PDTextView.swift
//  Advanced Aesthetic Dentistry
//
//  Created by Office on 2/24/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PDTextView: UITextView {

    override func drawRect(rect: CGRect) {
        super.drawRect(rect)
        layer.borderColor = borderColor.CGColor
        layer.borderWidth = borderWidth
        layer.cornerRadius = cornerRadius
        clipsToBounds = true
    }
    
    
    @IBInspectable var borderColor: UIColor = UIColor.blackColor() {
        didSet {
            layer.borderColor = borderColor.CGColor
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 3.0 {
        didSet {
            layer.cornerRadius = cornerRadius
            clipsToBounds = true
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 1.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }


}


class TextView: PDTextView {
    
    @IBInspectable var clearOnEndEditing: Bool = true
    var placeHolder : String? = ""
    var isEdited : Bool {
        get {
            return (self.text != placeHolder && !self.isEmpty)
        }
    }
    override func awakeFromNib() {
        if placeHolder?.isEmpty == true {
            placeHolder = self.text
        }
        if self.delegate == nil {
            self.delegate = self
        }
    }
    
    func checkEdited() {
        if isEdited {
            self.textColor = UIColor.blackColor()
        } else {
            self.text = placeHolder
            self.textColor = UIColor.lightGrayColor()
        }
    }
    
}

extension TextView : UITextViewDelegate {
    
    func textViewDidBeginEditing(textView: UITextView) {
        if textView.text == placeHolder {
            textView.text = ""
            textView.textColor = UIColor.blackColor()
        }
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if clearOnEndEditing || textView.text.isEmpty  {
            textView.text = placeHolder
            textView.textColor = UIColor.lightGrayColor()
        }
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}