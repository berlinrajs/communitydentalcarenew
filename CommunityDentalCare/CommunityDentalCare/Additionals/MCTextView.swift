//
//  MCTextView.swift
//  ProDental
//
//  Created by Berlin Raj on 20/09/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class MCTextView: UITextView {

//    var mCDelegate: UITextViewDelegate?
    
    override func drawRect(rect: CGRect) {
        super.drawRect(rect)
        
        layer.borderColor = borderColor.CGColor
        layer.borderWidth = borderWidth
        layer.cornerRadius = cornerRadius
        clipsToBounds = true
        
        self.autocorrectionType = UITextAutocorrectionType.No
        self.spellCheckingType = UITextSpellCheckingType.No
        self.autocapitalizationType = UITextAutocapitalizationType.AllCharacters
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        delegate = self
    }
    
    var borderColor: UIColor = UIColor.whiteColor() {
        didSet {
            layer.borderColor = borderColor.CGColor
        }
    }
    
    var cornerRadius: CGFloat = 3.0 {
        didSet {
            layer.cornerRadius = cornerRadius
            clipsToBounds = true
        }
    }
    
    var borderWidth: CGFloat = 1.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var placeholder: String? = nil {
        didSet {
            if placeholder?.characters.count > 0 {
                self.text = placeholder
                self.textColor = self.placeholderColor
            }
        }
    }
    @IBInspectable var defaultTextColor: UIColor = UIColor.blackColor() {
        didSet {
            if self.text != self.placeholder {
                self.textColor = defaultTextColor
            }
        }
    }
    
    @IBInspectable var placeholderColor: UIColor = UIColor.lightGrayColor() {
        didSet {
            if self.text == self.placeholder {
                self.textColor = placeholderColor
            }
        }
    }
    
    override var isEmpty : Bool {
        return text == placeholder ? true : super.isEmpty
    }
}
extension MCTextView: UITextViewDelegate {
    
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text == "" {
            textView.text = placeholder
            textView.textColor = placeholderColor
        }
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    func textViewDidBeginEditing(textView: UITextView) {
        if textView.text == placeholder {
            textView.text = ""
            textView.textColor = self.defaultTextColor
        }
    }
}
