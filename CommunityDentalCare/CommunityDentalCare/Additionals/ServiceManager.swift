//
//  ServiceManager.swift
//  Advanced Aesthetic Dentistry
//
//  Created by Bose on 02/02/16.
//  Copyright © 2016 Advanced Aesthetic Dentistry. All rights reserved.
//

import UIKit


class ServiceManager: NSObject {
    
    class func fetchDataFromService(baseUrlString: String, serviceName: String, parameters : [String : String]?, success:(result : AnyObject) -> Void, failure :(error : NSError) -> Void) {
        
        let manager = AFHTTPSessionManager(baseURL: NSURL(string: baseUrlString))
        
        manager.responseSerializer.acceptableContentTypes = ["text/html"]
        manager.POST(serviceName, parameters: parameters, progress: { (progress) in
            print(progress.fractionCompleted)
            }, success: { (task, result) in
                //                print(task)
                //                print(result)
                success(result: result!)
        }) { (task, error) in
            //            print(task)
            //            print(error)
            failure(error: error)
        }
    }
    
//    class func fetchDataFromServiceCheckIn(serviceName: String, parameters : [String : String]?, success:(result : AnyObject) -> Void, failure :(error : NSError) -> Void) {
//        
//        let manager = AFHTTPSessionManager(baseURL: NSURL(string: "http://alpha.mncell.com/mconsent/"))
//        
//        manager.responseSerializer.acceptableContentTypes = ["text/html"]
//        manager.POST(serviceName, parameters: parameters, progress: { (progress) in
//            print(progress.fractionCompleted)
//            }, success: { (task, result) in
//                //                print(task)
//                //                print(result)
//                success(result: result!)
//        }) { (task, error) in
//            //            print(task)
//            //            print(error)
//            failure(error: error)
//        }
//    }
    
    class func fetchDataFromService(baseUrlString: String, serviceName: String, parameters : [String : String]?, imageData: NSData?, success:(result : AnyObject) -> Void, failure :(error : NSError) -> Void) {
        
        let manager = AFHTTPSessionManager(baseURL: NSURL(string: baseUrlString))
        manager.responseSerializer.acceptableContentTypes = ["text/html"]
        
        manager.POST(serviceName, parameters: parameters, constructingBodyWithBlock: { (data) in
            if imageData != nil {
                data.appendPartWithFileData(imageData!, name: "task_data", fileName: "\(parameters!["patient_name"])_\(ServiceManager.date())_signature.jpeg", mimeType: "image/jpeg")
            }
            }, progress: { (progress) in
                
            }, success: { (task, result) in
                success(result: result!)
        }) { (task, error) in
            failure(error: error)
        }
    }

    
//    class func ChekInFormStatus(patientName: String, patientPurpose: String, completion: (success: Bool, error: NSError?) -> Void) {
//        //"mcdistinctivelaser"
//        ServiceManager.fetchDataFromServiceCheckIn("savepatients_info.php?", parameters: ["patientkey": "mcCommunityDental", "patientname": patientName, "patientpurpose": patientPurpose], success: { (result) in
//            if (result["posts"] as! String == "success") {
//                completion(success: true, error: nil)
//            } else {
//                completion(success: false, error: NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: (result["posts"]!)!["message"] as! String]))
//            }
//        }) { (error) in
//            completion(success: false, error: nil)
//        }
//    }
    
    class func date() -> String {
        let dateFormat = NSDateFormatter()
        dateFormat.dateFormat = "MMM_dd_yyyy_hh_mm"
        return dateFormat.stringFromDate(NSDate())
    }
    
    class func ChekInFormStatus(patientName: String, patientPurpose: String, signatureImage: UIImage?, completion: (success: Bool, error: NSError?) -> Void) {
        //"mcdistinctivelaser"
        ServiceManager.fetchDataFromService("http://alpha.mncell.com/mconsent/", serviceName:"savepatients_info.php?", parameters: ["patientkey": "mcCommunityDental", "patientname": patientName, "patientpurpose": patientPurpose], imageData: signatureImage == nil ? nil : UIImageJPEGRepresentation(signatureImage!, 0.2), success: { (result) in
            if (result["posts"] as! String == "success") {
                completion(success: true, error: nil)
            } else {
                completion(success: false, error: NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: (result["posts"]!)!["message"] as! String]))
            }
        }) { (error) in
            completion(success: false, error: nil)
        }
    }
    
    
    class func postReview(name: String, comment: String, rating: CGFloat, phoneNumber: String, allowMessage: Bool, email: String, anonymous: Bool, completion: (success: Bool, error: NSError?) -> Void) {
        //http://distinctdental.mncell.com/appservice/review.php
        ServiceManager.fetchDataFromService("https://alpha.mncell.com/review/", serviceName: "app_review.php?", parameters: ["patient_appkey": "mcCommunityDental", "patient_name": name, "patient_review_comment": comment, "patient_rating": "\(rating)", "patient_phone_number": phoneNumber, "sms_allowed": allowMessage == true ? "1" : "0", "patient_email": email, "user_anonymous": anonymous == true ? "1" : "0"], success: { (result) in
            if result["posts"] != nil && (result["posts"]!!)["status"] as! String == "success" {
                completion(success: true, error: nil)
            } else {
                completion(success: false, error: NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: (result["posts"]!!)["message"] as! String]))
            }
        }) { (error) in
            completion(success: false, error: NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Something went wrong, Please Check your internet connection and try again"]))
        }
    }

}
