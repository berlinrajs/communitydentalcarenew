//
//  PDLabel.swift
//  Advanced Aesthetic Dentistry
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PDLabel: UILabel {

    override func drawRect(rect: CGRect) {
        super.drawRect(rect)
        layer.borderColor = borderColor.CGColor
        layer.borderWidth = borderWidth
        layer.cornerRadius = cornerRadius
        clipsToBounds = true
    }
    
    
    @IBInspectable var borderColor: UIColor = UIColor.blackColor() {
        didSet {
            layer.borderColor =  UIColor.blackColor().CGColor//borderColor.CGColor
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 3.0 {
        didSet {
            layer.cornerRadius = cornerRadius
            clipsToBounds = true
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 1.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }

}
