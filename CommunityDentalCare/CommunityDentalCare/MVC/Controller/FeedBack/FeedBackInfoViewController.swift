//
//  FeedBackInfoViewController.swift
//  DistinctiveDentalCare
//
//  Created by Berlin Raj on 29/08/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class FeedBackInfoViewController: PDViewController {

    @IBOutlet weak var textFieldFirstName: PDTextField!
    @IBOutlet weak var textFieldLastName: PDTextField!
    @IBOutlet weak var textFieldPhoneNumber: PDTextField!
    @IBOutlet weak var labelDate: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate.text = patient.dateToday
        if patient.firstName != nil{
            textFieldFirstName.text = patient.firstName
            textFieldLastName.text = patient.lastName
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func buttonNextAction() {
        self.view.endEditing(true)
        if textFieldFirstName.isEmpty {
            let alert = Extention.alert("PLEASE ENTER YOUR FIRST NAME")
            self.presentViewController(alert, animated: true, completion: nil)
           
        } else if textFieldLastName.isEmpty {
            let alert = Extention.alert("PLEASE ENTER YOUR LAST NAME")
            self.presentViewController(alert, animated: true, completion: nil)

        } else if textFieldPhoneNumber.isEmpty || !textFieldPhoneNumber.text!.isPhoneNumber {
            
            let alert = Extention.alert("PLEASE ENTER A VALID PHONE NUBMER")
            self.presentViewController(alert, animated: true, completion: nil)
          
        } else {
            let feedBackVC = self.storyboard?.instantiateViewControllerWithIdentifier("kFeedBackViewController") as! FeedBackViewController
            feedBackVC.fullName = textFieldFirstName.text! + " " + textFieldLastName.text!
            feedBackVC.phoneNumber = textFieldPhoneNumber.text!.phoneNumber
            self.navigationController?.pushViewController(feedBackVC, animated: true)
        }
    }
}
extension FeedBackInfoViewController: UITextFieldDelegate {
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldPhoneNumber {
            return textField.formatPhoneNumber(range, string: string)
        }
        return true
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
}
