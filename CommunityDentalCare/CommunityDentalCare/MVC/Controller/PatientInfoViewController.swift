//
//  PatientInfoViewController.swift
//  Advanced Aesthetic Dentistry
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientInfoViewController: PDViewController {
    
    @IBOutlet weak var labelDate: UILabel!
   // @IBOutlet var datePicker: UIDatePicker!
    @IBOutlet weak var textFieldFirstName: PDTextField!
    @IBOutlet weak var textFieldLastName: PDTextField!
//    @IBOutlet weak var textfieldMiddleInitial : UITextField!
   // @IBOutlet weak var textFieldDateOfBirth: PDTextField!
 //   @IBOutlet var toolBar: UIToolbar!
    
    
    @IBOutlet weak var textfieldMonth : UITextField!
    @IBOutlet weak var textfieldDate : UITextField!
    @IBOutlet weak var textfieldYear : UITextField!

    
    override func viewDidLoad() {
        super.viewDidLoad()

      
        
//        datePicker.maximumDate = NSDate()
//        textFieldDateOfBirth.inputView = datePicker
//        textFieldDateOfBirth.inputAccessoryView = toolBar
        MonthInputView.addMonthPickerForTextField(textfieldMonth)

        labelDate.text = patient.dateToday
        if patient.firstName != nil{
            textFieldFirstName.text = patient.firstName
            textFieldLastName.text = patient.lastName
        }
        
         }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonActionNext(sender : AnyObject) {
        self.view.endEditing(true)
        if textFieldFirstName.isEmpty {
            let alert = Extention.alert("PLEASE ENTER PATIENT FIRST NAME")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if textFieldLastName.isEmpty {
            let alert = Extention.alert("PLEASE ENTER PATIENT LAST NAME")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if invalidDateofBirth {
            let alert = Extention.alert("PLEASE ENTER THE VALID DATE OF BIRTH")
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            
            patient.firstName = textFieldFirstName.text
            patient.lastName = textFieldLastName.text
            patient.initial = "" //textfieldMiddleInitial.isEmpty ? "" : textfieldMiddleInitial.text!
            patient.dateOfBirth = textfieldMonth.text! + " " + textfieldDate.text! + ", " + textfieldYear.text!
            
            
                self.gotoNextForm(true)
          
        }
    }
    
    
    var invalidDateofBirth: Bool {
        get {
            if textfieldMonth.isEmpty || textfieldDate.isEmpty || textfieldYear.isEmpty {
                return true
            } else if Int(textfieldDate.text!)! == 0{
                return true
            } else if !textfieldYear.text!.isValidYear {
                return true
            } else {
                
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "dd-MMM-yyyy"
                
                let todayDate = dateFormatter.dateFromString(dateFormatter.stringFromDate(NSDate()))
                let currentDate = dateFormatter.dateFromString("\(textfieldDate.text!)-\(textfieldMonth.text!)-\(textfieldYear.text!)")
                
                if todayDate == nil || currentDate == nil {
                    return true
                }
                if todayDate!.timeIntervalSinceDate(currentDate!) < 0 {
                    return true
                }
                return false
            }
        }
    }
    

//    @IBAction func toolbarDoneButtonAction(sender: AnyObject) {
//        textFieldDateOfBirth.resignFirstResponder()
//        let dateFormatter = NSDateFormatter()
//        dateFormatter.dateFormat = "MMMM dd, yyyy"
//        textFieldDateOfBirth.text = dateFormatter.stringFromDate(datePicker.date).uppercaseString
//    }
//
//    @IBAction func datePickerDateChanged(sender: AnyObject) {
//        let dateFormatter = NSDateFormatter()
//        dateFormatter.dateFormat = "MMMM dd, yyyy"
//        textFieldDateOfBirth.text = dateFormatter.stringFromDate(datePicker.date).uppercaseString
//    }
//    
    

}

extension PatientInfoViewController : UITextFieldDelegate {
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
//        if textField == textfieldMiddleInitial {
//            return textField.formatInitial(range, string: string)
//        }else
        if textField == textfieldDate{
            return textField.formatDate(range, string: string)
        } else if textField == textfieldYear{
            return textField.formatNumbers(range, string: string, count: 4)
        }
        return true
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

