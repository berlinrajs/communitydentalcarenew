//
//  MedicalHistory2ViewController.swift
//  CommunityDentalCare
//
//  Created by Bala Murugan on 9/27/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class MedicalHistory2ViewController: PDViewController {
    
    @IBOutlet var tableViewQuestions: UITableView!
    @IBOutlet var buttonVerified: UIButton!
    @IBOutlet var buttonNext: UIButton!
    @IBOutlet var buttonBack1: UIButton!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var labelHeading : UILabel!
    var selectedIndex: Int = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.activityIndicator.stopAnimating()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onBackButton1Pressed (withSender sender: AnyObject) {
        if selectedIndex == 0 {
            self.navigationController?.popViewControllerAnimated(true)
        } else {
            buttonBack1.userInteractionEnabled = false
            buttonNext.userInteractionEnabled = false
            
            self.activityIndicator.startAnimating()
            let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.4 * Double(NSEC_PER_SEC)))
            dispatch_after(delayTime, dispatch_get_main_queue()) {
                self.activityIndicator.stopAnimating()
                self.selectedIndex = self.selectedIndex - 1
                self.labelHeading.text = self.selectedIndex == 0 ? "" : "Have you ever had any of the following: *"
                self.tableViewQuestions.reloadData()
                self.buttonBack1.userInteractionEnabled = true
                self.buttonNext.userInteractionEnabled = true
            }
            
        }
    }
    
    @IBAction func buttonVerifiedAction(withSender sender: UIButton) {
        sender.selected = !sender.selected
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if buttonVerified.selected == false {
            let alert = Extention.alert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            if selectedIndex == 2 {
                let medical = medicalStoryboard.instantiateViewControllerWithIdentifier("Medical3VC") as! MedicalHistory3ViewController
                medical.patient = self.patient
                self.navigationController?.pushViewController(medical, animated: true)
                
            } else {
                buttonBack1.userInteractionEnabled = false
                buttonNext.userInteractionEnabled = false
                self.buttonVerified.selected = false
                self.activityIndicator.startAnimating()
                let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.4 * Double(NSEC_PER_SEC)))
                dispatch_after(delayTime, dispatch_get_main_queue()) {
                    self.activityIndicator.stopAnimating()
                    self.selectedIndex = self.selectedIndex + 1
                    self.labelHeading.text = self.selectedIndex == 0 ? "" : "Have you ever had any of the following: *"
                    self.tableViewQuestions.reloadData()
                    self.buttonBack1.userInteractionEnabled = true
                    self.buttonNext.userInteractionEnabled = true
                }
            }
        }
    }
    
}

extension MedicalHistory2ViewController : UITableViewDataSource,UITableViewDelegate {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patient.medicalHistory.medicalQuestions[selectedIndex].count
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! PatientInfoCell
        cell.delegate = self
        cell.tag = indexPath.row
        cell.configCell(self.patient.medicalHistory.medicalQuestions[selectedIndex][indexPath.row])
        
        return cell
    }
}

extension MedicalHistory2ViewController : PatientInfoCellDelegate{
    
    func radioButtonTappedForCell(cell: PatientInfoCell) {
        if selectedIndex == 0 {
            if cell.tag == 2{
                PopupTextView.sharedInstance.showWithPlaceHolder("PLEASE SPECIFY", completion: { (textView, isEdited) in
                    if isEdited{
                        self.patient.medicalHistory.medicalQuestions[self.selectedIndex][cell.tag].answer = textView.text
                    }else{
                        cell.radioButtonYes.selected = false
                        self.patient.medicalHistory.medicalQuestions[self.selectedIndex][cell.tag].selectedOption = false
                    }
                })
            }else if cell.tag == 3{
                MedicationAlert.sharedInstance.showPopUp(self.view, completion: { (arrayTags, other) in
                    if arrayTags.count == 0{
                        cell.radioButtonYes.selected = false
                        self.patient.medicalHistory.medicationsTag = [Int]()
                        self.patient.medicalHistory.medicationOthers = "N/A"
                        self.patient.medicalHistory.medicalQuestions[self.selectedIndex][cell.tag].selectedOption = false

                        
                    }else{
                        self.patient.medicalHistory.medicationsTag = arrayTags
                        self.patient.medicalHistory.medicationOthers = other
                        
                    }
                    
                    }, showAlert: { (alertMessage) in
                        let alert = Extention.alert(alertMessage)
                        self.presentViewController(alert, animated: true, completion: nil)
                })
            }else if cell.tag == 9{
                YesOrNoAlert.sharedInstance.showWithTitle("Do you have high or low blood pressure?", button1Title: "HIGH", button2Title: "LOW", completion: { (buttonIndex) in
                    self.patient.medicalHistory.medicalQuestions[self.selectedIndex][cell.tag].answer = buttonIndex == 1 ? "HIGH" : "LOW"
                    
                })
                
            }
        }else if selectedIndex == 2{
            if cell.tag == 11{
                PopupTextView.sharedInstance.showWithPlaceHolder("PLEASE SPECIFY", completion: { (textView, isEdited) in
                    if isEdited{
                        self.patient.medicalHistory.medicalQuestions[self.selectedIndex][cell.tag].answer = textView.text
                    }else{
                        cell.radioButtonYes.selected = false
                        self.patient.medicalHistory.medicalQuestions[self.selectedIndex][cell.tag].selectedOption = false

                    }
                })
            }
            
        }
        
    }
}

