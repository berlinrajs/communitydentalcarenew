//
//  MedicalHistory3ViewController.swift
//  CommunityDentalCare
//
//  Created by Bala Murugan on 9/27/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class MedicalHistory3ViewController: PDViewController {
    
    @IBOutlet weak var radioWoman : RadioButton!
    @IBOutlet weak var viewContainer : UIView!
    @IBOutlet weak var radioPregnant : RadioButton!
    @IBOutlet var arrayAllergyButtons : [UIButton]!
    @IBOutlet var arrayTestButtons : [UIButton]!
    @IBOutlet var arraySmokeButtons : [UIButton]!

    override func viewDidLoad() {
        super.viewDidLoad()
//        patient.medicalHistory.arrayAllergyTags = [Int]()
//        patient.medicalHistory.arrayTestTags = [Int]()
//        patient.medicalHistory.allergyOthers = "N/A"
        
        loadValues()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func saveValues (){
        patient.medicalHistory.radioPregnantTag = radioPregnant.selectedButton == nil ? 0 : radioPregnant.selectedButton.tag
        patient.medicalHistory.radioWomanTag = radioWoman.selectedButton.tag

    }
    
    func loadValues()  {
        for btn in arrayAllergyButtons{
            btn.selected = patient.medicalHistory.arrayAllergyTags.contains(btn.tag)
        }
        for btn in arrayTestButtons{
            btn.selected = patient.medicalHistory.arrayTestTags.contains(btn.tag)
        }
        for btn in arraySmokeButtons{
            btn.selected = patient.medicalHistory.arraySmokeTag.contains(btn.tag)
        }
        radioWoman.setSelectedWithTag(patient.medicalHistory.radioWomanTag)
        radioPregnant.setSelectedWithTag(patient.medicalHistory.radioPregnantTag)
        if radioWoman.selectedButton.tag == 1{
            viewContainer.userInteractionEnabled = true
            viewContainer.alpha = 1.0
        }else{
            viewContainer.userInteractionEnabled = false
            viewContainer.alpha = 0.5
            radioPregnant.deselectAllButtons()
            self.patient.medicalHistory.radioBirthControlTag = 0
            self.patient.medicalHistory.radioPregnantTag = 0
            
        }



    }
    
    @IBAction override func buttonActionBack(withSender sender : UIButton) {
        saveValues()
        super.buttonActionBack(withSender: sender)
    }

    
    @IBAction func allergyButtonAction (withSender sender : UIButton){
        sender.selected = !sender.selected
        if patient.medicalHistory.arrayAllergyTags.contains(sender.tag){
            patient.medicalHistory.arrayAllergyTags.removeAtIndex(patient.medicalHistory.arrayAllergyTags.indexOf(sender.tag)!)
        }else{
            patient.medicalHistory.arrayAllergyTags.append(sender.tag)
        }
        
        if sender.tag == 8 && sender.selected == true{
            PopupTextView.sharedInstance.showWithPlaceHolder("PLEASE SPECIFY", completion: { (textView, isEdited) in
                if isEdited{
                    self.patient.medicalHistory.allergyOthers = textView.text!
                    
                }else{
                    self.patient.medicalHistory.allergyOthers = "N/A"
                    sender.selected = false
                    self.patient.medicalHistory.arrayAllergyTags.removeAtIndex(self.patient.medicalHistory.arrayAllergyTags.indexOf(sender.tag)!)
                }
            })
            
        }else if sender.tag == 8 && sender.selected == false{
            self.patient.medicalHistory.allergyOthers = "N/A"
        }
        
    }
    
    @IBAction func testButtonAction (withSender sender : UIButton){
        sender.selected = !sender.selected
        if patient.medicalHistory.arrayTestTags.contains(sender.tag){
            patient.medicalHistory.arrayTestTags.removeAtIndex(patient.medicalHistory.arrayTestTags.indexOf(sender.tag)!)
        }else{
            patient.medicalHistory.arrayTestTags.append(sender.tag)
        }

    }
    
    @IBAction func smokeButtonAction (withSender sender : UIButton){
        sender.selected = !sender.selected
        if patient.medicalHistory.arraySmokeTag.contains(sender.tag){
            patient.medicalHistory.arraySmokeTag.removeAtIndex(patient.medicalHistory.arraySmokeTag.indexOf(sender.tag)!)
        }else{
            patient.medicalHistory.arraySmokeTag.append(sender.tag)
        }

    }
    
    @IBAction func radioWomanButtonAction (withSender sender : RadioButton){
        if sender.tag == 1{
            viewContainer.userInteractionEnabled = true
            viewContainer.alpha = 1.0
        }else{
            viewContainer.userInteractionEnabled = false
            viewContainer.alpha = 0.5
            radioPregnant.deselectAllButtons()
            self.patient.medicalHistory.radioBirthControlTag = 0
            self.patient.medicalHistory.radioPregnantTag = 0


        }
        
    }
    
    @IBAction func radioPregnantButtonAction (withSender sender : UIButton){
        if sender.tag == 2{
        YesOrNoAlert.sharedInstance.showWithTitle("Are you taking birth control?", button1Title: "YES", button2Title: "NO") { (buttonIndex) in
            self.patient.medicalHistory.radioBirthControlTag = buttonIndex
        }
        }else{
            self.patient.medicalHistory.radioBirthControlTag = 0

        }
    }

    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if radioWoman.selectedButton.tag == 1 && radioPregnant.selectedButton == nil{
            let alert = Extention.alert("PLEASE ENTER ALL THE REQUIRED FIELDS")
            self.presentViewController(alert, animated: true, completion: nil)

        }else{
            saveValues()
            let medical = medicalStoryboard.instantiateViewControllerWithIdentifier("Medical4VC") as! MedicalHistory4ViewController
            medical.patient = self.patient
            self.navigationController?.pushViewController(medical, animated: true)

        }
        
    }
}
