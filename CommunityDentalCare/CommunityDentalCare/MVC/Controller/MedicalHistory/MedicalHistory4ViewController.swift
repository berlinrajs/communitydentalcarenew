//
//  MedicalHistory4ViewController.swift
//  CommunityDentalCare
//
//  Created by Bala Murugan on 9/27/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class MedicalHistory4ViewController: PDViewController {
    @IBOutlet weak var textviewComments : UITextView!
    @IBOutlet weak var signaturePatient : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!
    @IBOutlet weak var labelPatientName : UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate.todayDate = patient.dateToday
        labelPatientName.text = patient.fullName
        loadValues()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func saveValues (){
        patient.medicalHistory.patientComments = textviewComments.text! == "PLEASE TYPE HERE" ? "N/A" : textviewComments.text!

    }
    
    func loadValues()  {
        textviewComments.text = patient.medicalHistory.patientComments == "" ? "PLEASE TYPE HERE" : patient.medicalHistory.patientComments
        if textviewComments.text == "PLEASE TYPE HERE"{
            textviewComments.textColor = UIColor.lightGrayColor()
        }else{
            textviewComments.textColor = UIColor.blackColor()
        }

    }
    
    @IBAction override func buttonActionBack(withSender sender : UIButton) {
        saveValues()
        super.buttonActionBack(withSender: sender)
    }

    

    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !signaturePatient.isSigned(){
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.presentViewController(alert, animated: true, completion: nil)

        }else if !labelDate.dateTapped{
            let alert = Extention.alert("PLEASE SELECT THE DATE")
            self.presentViewController(alert, animated: true, completion: nil)

        }else{
            saveValues()
            //patient.medicalHistory.patientComments = textviewComments.text! == "PLEASE TYPE HERE" ? "N/A" : textviewComments.text!
            patient.medicalHistory.patientSignature = signaturePatient.signatureImage()
            let medical = medicalStoryboard.instantiateViewControllerWithIdentifier("MedicalFormVC") as! MedicalHistoryFormViewController
            medical.patient = self.patient
            self.navigationController?.pushViewController(medical, animated: true)

        }
        
    }

}

extension MedicalHistory4ViewController : UITextViewDelegate {
    
    func textViewDidBeginEditing(textView: UITextView) {
        if textView.text == "PLEASE TYPE HERE" {
            textView.text = ""
            textView.textColor = UIColor.blackColor()
        }
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "PLEASE TYPE HERE"
            textView.textColor = UIColor.lightGrayColor()
        }
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}
