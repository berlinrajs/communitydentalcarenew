//
//  MedicalHistoryFormViewController.swift
//  CommunityDentalCare
//
//  Created by Bala Murugan on 9/28/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class MedicalHistoryFormViewController: PDViewController {
    
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelBirthDate : UILabel!
    @IBOutlet weak var labelBp : UILabel!
    @IBOutlet weak var labelPulse : UILabel!
    @IBOutlet weak var labelPhysicianName : UILabel!
    @IBOutlet weak var labelPhysicianPhone : UILabel!
    @IBOutlet weak var labelPhysicianAddress : UILabel!
    @IBOutlet weak var labelLastDentalExam : UILabel!
    @IBOutlet      var arrayRadioButtons : [RadioButton]!
    @IBOutlet      var arraymedicationButtons : [UIButton]!
    @IBOutlet weak var labelMedicationOther : UILabel!
    @IBOutlet      var arrayAllergyButtons : [UIButton]!
    @IBOutlet weak var labelAllergyOthers : UILabel!
    @IBOutlet      var arrayTestButtons : [UIButton]!
    @IBOutlet      var arrayMedicalHistoryButtons1 : [UIButton]!
    @IBOutlet      var arrayMedicalHistoryButtons2 : [UIButton]!
    @IBOutlet weak var labelMedicalHistoryOther : UILabel!
    @IBOutlet      var arraySmokeButtons : [UIButton]!
    @IBOutlet weak var radioPregnant : RadioButton!
    @IBOutlet weak var radioBirthControl : RadioButton!
    @IBOutlet weak var textviewComments : UITextView!
    @IBOutlet      var labelOfficeComments : [UILabel]!
    @IBOutlet weak var signatureOffice : UIImageView!
    @IBOutlet weak var signaturePatient : UIImageView!
    @IBOutlet weak var labelDate1 : UILabel!
    @IBOutlet weak var labelDate2 : UILabel!
    @IBOutlet weak var labelBloodPressure : UILabel!
    @IBOutlet weak var radioAllergy : RadioButton!
    @IBOutlet weak var radioTest : RadioButton!
    @IBOutlet weak var labelSurgery : UILabel!


    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func loadData() {
        labelPatientName.text = patient.fullName
        labelBirthDate.text = patient.dateOfBirth
        labelBp.text = patient.medicalHistory.bloodpressureLow == "" && patient.medicalHistory.bloodpressureHigh == "" ? "N/A" : patient.medicalHistory.bloodpressureLow + " - " + patient.medicalHistory.bloodpressureHigh
        labelPulse.text = patient.medicalHistory.pulse
        labelPhysicianName.text = patient.medicalHistory.physicianName
        labelPhysicianPhone.text = patient.medicalHistory.PhysicianPhone
        labelPhysicianAddress.text = patient.medicalHistory.physicianAddress
        labelLastDentalExam.text = patient.medicalHistory.lastDentalExam
        for btn in arrayRadioButtons{
            let ques : PDQuestion = patient.medicalHistory.medicalQuestions[0][btn.tag]
            btn.selected = ques.selectedOption == true
        }
        for btn in arraymedicationButtons{
            btn.selected = patient.medicalHistory.medicationsTag.contains(btn.tag)
        }
        labelMedicationOther.text = patient.medicalHistory.medicationOthers
        for btn in arrayAllergyButtons{
            btn.selected = patient.medicalHistory.arrayAllergyTags.contains(btn.tag)

        }
        labelAllergyOthers.text = patient.medicalHistory.allergyOthers
        for btn in arrayTestButtons{
            btn.selected = patient.medicalHistory.arrayTestTags.contains(btn.tag)
        }
        for btn in arrayMedicalHistoryButtons1{
            let ques : PDQuestion = patient.medicalHistory.medicalQuestions[1][btn.tag]
            btn.selected = ques.selectedOption == true
        }
        for btn in arrayMedicalHistoryButtons2{
            let ques : PDQuestion = patient.medicalHistory.medicalQuestions[2][btn.tag]
            btn.selected = ques.selectedOption == true
        }

        labelMedicalHistoryOther.text = patient.medicalHistory.medicalQuestions[2][11].answer
        for btn in arraySmokeButtons{
            btn.selected = patient.medicalHistory.arraySmokeTag.contains(btn.tag)
        }
        radioPregnant.setSelectedWithTag(patient.medicalHistory.radioPregnantTag)
        radioBirthControl.setSelectedWithTag(patient.medicalHistory.radioBirthControlTag)
        textviewComments.text = patient.medicalHistory.patientComments
        patient.medicalHistory.medicalHistoryOfficeComments.setTextForArrayOfLabels(labelOfficeComments)
        signatureOffice.image = patient.medicalHistory.medicalHistoryOfficeSign
        signaturePatient.image = patient.medicalHistory.patientSignature
        labelDate1.text = patient.dateToday
        labelDate2.text = patient.medicalHistory.medicalHistoryOfficeComments == "" ? "" : patient.dateToday
        
        labelSurgery.text = patient.medicalHistory.medicalQuestions[0][2].answer
        labelBloodPressure.text = patient.medicalHistory.medicalQuestions[0][9].answer
        radioTest.selected = patient.medicalHistory.arrayTestTags.count > 0
        radioAllergy.selected = patient.medicalHistory.arrayAllergyTags.count > 0
    }

}
