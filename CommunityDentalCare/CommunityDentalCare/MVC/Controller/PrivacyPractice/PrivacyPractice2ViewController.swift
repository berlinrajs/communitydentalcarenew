//
//  PrivacyPractice2ViewController.swift
//  CommunityDentalCare
//
//  Created by Bala Murugan on 9/26/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PrivacyPractice2ViewController: PDViewController {
    @IBOutlet weak var textfieldName : UITextField!
    @IBOutlet weak var textfieldRelationship : UITextField!
    @IBOutlet weak var viewContainer : UIView!
    @IBOutlet weak var signatureViewPatient : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var radioPatient : RadioButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate.todayDate = patient.dateToday
        labelPatientName.text = patient.fullName

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func onPersonalRepresentativePressed (withSender sender : RadioButton){
        if sender.tag == 1{
            viewContainer.userInteractionEnabled = false
            viewContainer.alpha = 0.5
            textfieldName.text = ""
            textfieldRelationship.text = ""
            labelPatientName.text = patient.fullName
        }else{
            viewContainer.userInteractionEnabled = true
            viewContainer.alpha = 1.0
            labelPatientName.text = ""
        }
        
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        
        if radioPatient.selectedButton.tag == 2 && (textfieldRelationship.isEmpty || textfieldName.isEmpty){
            let alert = Extention.alert("PLEASE ENTER ALL THE REQUIRED FIELDS")
            self.presentViewController(alert, animated: true, completion: nil)
        }else if !signatureViewPatient.isSigned(){
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.presentViewController(alert, animated: true, completion: nil)
        }else if !labelDate.dateTapped{
            let alert = Extention.alert("PLEASE SELECT THE DATE")
            self.presentViewController(alert, animated: true, completion: nil)
        }else{
            patient.privacyRepresentativeName = radioPatient.selectedButton.tag == 1 ? "N/A" : textfieldName.text!
            patient.privacyRepresentativeRelationship = radioPatient.selectedButton.tag == 1 ? "N/A" : textfieldRelationship.text!
            patient.privacySignature = signatureViewPatient.signatureImage()
            let step3VC = patientStoryboard.instantiateViewControllerWithIdentifier("PatientRegistrationForm") as! PatientRegistrationForm
            step3VC.patient = self.patient
            self.navigationController?.pushViewController(step3VC, animated: true)

//            let privacy = mainStoryboard.instantiateViewControllerWithIdentifier("PrivacyFormVC") as! PrivacyPracticeFormViewController
//            privacy.patient = self.patient
//            self.navigationController?.pushViewController(privacy, animated: true)
        }
    }

}

extension PrivacyPractice2ViewController: UITextFieldDelegate {
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
}
