//
//  PrivacyPractice1ViewController.swift
//  CommunityDentalCare
//
//  Created by Bala Murugan on 9/26/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PrivacyPractice1ViewController: PDViewController {
    
    @IBOutlet weak var textfieldAddress : UITextField!
    @IBOutlet weak var textfieldCity : UITextField!
    @IBOutlet weak var textfieldState : UITextField!
    @IBOutlet weak var textfieldZipcode : UITextField!
    @IBOutlet weak var textfieldPhoneNumber : UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        StateListView.addStateListForTextField(textfieldState)
            textfieldAddress.text = patient.address
            textfieldCity.text = patient.city
            textfieldState.text = patient.state == "" ? "MN" : patient.state
            textfieldZipcode.text = patient.zipcode
            textfieldPhoneNumber.text = patient.cellPhoneNumber == "N/A" ? "" : patient.cellPhoneNumber
        
       loadValues()
    }

    func saveValues (){
        patient.privacyAddress = textfieldAddress.isEmpty ? "" : textfieldAddress.text!
        patient.privacyCity = textfieldCity.isEmpty ? "" : textfieldCity.text!
        patient.privacyState = textfieldState.isEmpty ? "" : textfieldState.text!
        patient.privacyZipcode = textfieldZipcode.isEmpty ? "" : textfieldZipcode.text!
        patient.privacyPhoneNumber = textfieldPhoneNumber.isEmpty ? "N/A" : textfieldPhoneNumber.text!
    }
    
    func loadValues()  {
        textfieldAddress.setSavedText(patient.privacyAddress)
        textfieldCity.setSavedText(patient.privacyCity)
        textfieldState.text = patient.privacyState == "" ? "MN" : patient.privacyState
        textfieldZipcode.setSavedText(patient.privacyZipcode)
        textfieldPhoneNumber.setSavedText(patient.privacyPhoneNumber)
        
    }
    
    @IBAction override func buttonActionBack(withSender sender : UIButton) {
        saveValues()
        super.buttonActionBack(withSender: sender)
        ///self.navigationController?.popViewControllerAnimated(true)
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if textfieldAddress.isEmpty || textfieldCity.isEmpty || textfieldState.isEmpty || textfieldZipcode.isEmpty{
            let alert = Extention.alert("PLEASE ENTER ALL THE REQUIRED FIELDS")
            self.presentViewController(alert, animated: true, completion: nil)
        }else if !textfieldZipcode.text!.isZipCode{
            let alert = Extention.alert("PLEASE ENTER THE VALID ZIPCODE")
            self.presentViewController(alert, animated: true, completion: nil)
        }else if !textfieldPhoneNumber.isEmpty && !textfieldPhoneNumber.text!.isPhoneNumber{
            let alert = Extention.alert("PLEASE ENTER THE VALID PHONE NUMBER")
            self.presentViewController(alert, animated: true, completion: nil)
        }else{
            saveValues()
            let privacy = mainStoryboard.instantiateViewControllerWithIdentifier("Privacy2VC") as! PrivacyPractice2ViewController
            privacy.patient = self.patient
            self.navigationController?.pushViewController(privacy, animated: true)
        }
    }


}

extension PrivacyPractice1ViewController: UITextFieldDelegate {
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == textfieldPhoneNumber {
            return textField.formatPhoneNumber(range, string: string)
        }else if textField == textfieldZipcode{
            return textField.formatZipCode(range, string: string)
        }
        return true
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
}
