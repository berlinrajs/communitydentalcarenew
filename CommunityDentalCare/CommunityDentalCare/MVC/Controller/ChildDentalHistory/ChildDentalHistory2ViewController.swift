//
//  ChildDentalHistory2ViewController.swift
//  CommunityDentalCare
//
//  Created by Bala Murugan on 9/29/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ChildDentalHistory2ViewController: PDViewController {

    @IBOutlet weak var textviewReason : UITextView!
    @IBOutlet weak var textfieldTreatment : UITextField!
    @IBOutlet weak var textfieldTreatmentDate: UITextField!
    @IBOutlet weak var radioFear : RadioButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        DateInputView.addDatePickerForTextField(textfieldTreatmentDate)
        loadValues()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func saveValues (){
        patient.childDentalHistory.reasonVisit = textviewReason.text! == "PLEASE TYPE HERE" ? "" : textviewReason.text!
        patient.childDentalHistory.treatment = textfieldTreatment.isEmpty ? "N/A" : textfieldTreatment.text!
        patient.childDentalHistory.treatmentDate = textfieldTreatmentDate.isEmpty ? "N/A" : textfieldTreatmentDate.text!
        patient.childDentalHistory.radioFearTag = radioFear.selectedButton == nil ? 0 : radioFear.selectedButton.tag
        
    }
    
    func loadValues()  {
        textviewReason.text = patient.childDentalHistory.reasonVisit == "" ? "PLEASE TYPE HERE" : patient.childDentalHistory.reasonVisit
        if textviewReason.text == "PLEASE TYPE HERE"{
            textviewReason.textColor = UIColor.lightGrayColor()
        }else{
            textviewReason.textColor = UIColor.blackColor()
        }
        textfieldTreatment.setSavedText(patient.childDentalHistory.treatment)
        textfieldTreatmentDate.setSavedText(patient.childDentalHistory.treatmentDate)
        radioFear.setSelectedWithTag(patient.childDentalHistory.radioFearTag)
        
    }
    
    @IBAction override func buttonActionBack(withSender sender : UIButton) {
        saveValues()
        super.buttonActionBack(withSender: sender)
    }

    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if textviewReason.text == "PLEASE TYPE HERE"{
            let alert = Extention.alert("PLEASE ENTER ALL THE REQUIRED FIELDS")
            self.presentViewController(alert, animated: true, completion: nil)

        }else{
            saveValues()
            let dental = childMedicalStoryboard.instantiateViewControllerWithIdentifier("ChildDental3VC") as! ChildDentalHistory3ViewController
            dental.patient = self.patient
            self.navigationController?.pushViewController(dental, animated: true)
        }
        
    }
    

}
extension ChildDentalHistory2ViewController : UITextViewDelegate {
    
    func textViewDidBeginEditing(textView: UITextView) {
        if textView.text == "PLEASE TYPE HERE" {
            textView.text = ""
            textView.textColor = UIColor.blackColor()
        }
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "PLEASE TYPE HERE"
            textView.textColor = UIColor.lightGrayColor()
        }
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}

extension ChildDentalHistory2ViewController: UITextFieldDelegate {
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
}
