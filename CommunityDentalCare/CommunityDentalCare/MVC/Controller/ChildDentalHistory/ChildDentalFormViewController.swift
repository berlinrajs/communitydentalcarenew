//
//  ChildDentalFormViewController.swift
//  CommunityDentalCare
//
//  Created by Bala Murugan on 9/29/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ChildDentalFormViewController: PDViewController {
    
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelDateOfBirth : UILabel!
    @IBOutlet weak var labelReason : UILabel!
    @IBOutlet      var arrayRadioButtons1 : [RadioButton]!
    @IBOutlet      var arrayRadioButtons2 : [RadioButton]!
    @IBOutlet weak var labelTreatment : UILabel!
    @IBOutlet weak var labelTreatmentDate : UILabel!
    @IBOutlet weak var radioButtonFear : RadioButton!
    @IBOutlet weak var textviewComments : UITextView!
    @IBOutlet      var arrayLabelOfficeComments : [UILabel]!
    @IBOutlet weak var signaturePatient : UIImageView!
    @IBOutlet weak var signatureOffice : UIImageView!
    @IBOutlet weak var labelDate1 : UILabel!
    @IBOutlet weak var labelDate2 : UILabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadData(){
        labelPatientName.text = patient.fullName
        labelDateOfBirth.text = patient.dateOfBirth
        for btn in arrayRadioButtons1{
            btn.selected = patient.childDentalHistory.medicalQuestions1[0][btn.tag].selectedOption == true
        }
        for btn in arrayRadioButtons2{
            btn.selected = patient.childDentalHistory.medicalQuestions1[1][btn.tag].selectedOption == true
        }

        labelReason.text = patient.childDentalHistory.reasonVisit
        labelTreatment.text = patient.childDentalHistory.treatment
        labelTreatmentDate.text = patient.childDentalHistory.treatmentDate
        radioButtonFear.setSelectedWithTag(patient.childDentalHistory.radioFearTag)
        textviewComments.text = patient.childDentalHistory.patientComments
        patient.childDentalHistory.childDentalHistoryOfficeComments.setTextForArrayOfLabels(arrayLabelOfficeComments)
        signaturePatient .image = patient.childDentalHistory.patientSignature
        signatureOffice.image = patient.childDentalHistory.childDentalHistoryOfficeSign
        labelDate1.text = patient.dateToday
        labelDate2.text = patient.childDentalHistory.childDentalHistoryOfficeComments == "" ? "" : patient.dateToday
    }


}
