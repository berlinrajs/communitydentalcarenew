//
//  PDViewController.swift
//  Advanced Aesthetic Dentistry
//
//  Created by Leojin Bose on 2/22/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PDViewController: UIViewController {
    
    var patient : PDPatient!
    
    @IBOutlet weak var pdfView: UIScrollView?
    
    
    @IBOutlet weak var buttonBack: UIButton?
    @IBOutlet weak var buttonSubmit: UIButton?
    var isFromPatientInfo: Bool = false
    
    let medicalStoryboard = UIStoryboard(name: "MedicalHistory", bundle: NSBundle.mainBundle())
    let childMedicalStoryboard = UIStoryboard(name: "ChildMedicalHistory", bundle: NSBundle.mainBundle())
    let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
    let patientStoryboard = UIStoryboard(name : "Patient", bundle: NSBundle.mainBundle())


    override func viewDidLoad() {
        super.viewDidLoad()
        edgesForExtendedLayout = .None
        self.buttonBack?.hidden = self.buttonSubmit == nil && isFromPreviousForm
        buttonSubmit?.backgroundColor = UIColor.greenColor()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
    }
    var isFromPreviousForm: Bool {
        get {
            return navigationController?.viewControllers.count > 3 ? true : false
        }
    }
    
    @IBAction func buttonActionBack(withSender sender : UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
//    @IBAction func buttonBackAction(sender: AnyObject) {
//        buttonActionBack(sender)
//    }
//    
    
    func showAlert(message: String) {
        let alert = Extention.alert(message)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func showAlert1(message: String, completion: (completed: Bool) -> Void) {
        let alertController = UIAlertController(title: "Community Dental Care", message: message, preferredStyle: UIAlertControllerStyle.Alert)
        let alertOkAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Destructive) { (action) -> Void in
            completion(completed: true)
        }
        alertController.addAction(alertOkAction)
        self.presentViewController(alertController, animated: true, completion: nil)
    }

    
    @IBAction func buttonActionSubmit(withSender sender : UIButton) {
        if !Reachability.isConnectedToNetwork() {
            let alertController = UIAlertController(title: "Community Dental Care", message: "Your device is not connected to internet. Please go to settings to connect.", preferredStyle: UIAlertControllerStyle.Alert)
            let alertOkAction = UIAlertAction(title: "Settings", style: UIAlertActionStyle.Destructive) { (action) -> Void in
                let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
                if let url = settingsUrl {
                    UIApplication.sharedApplication().openURL(url)
                }
            }
            let alertCancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Destructive) { (action) -> Void in
                
            }
            alertController.addAction(alertOkAction)
            alertController.addAction(alertCancelAction)
            self.presentViewController(alertController, animated: true, completion: nil)
            return
        }
        let pdfManager = PDFManager()
        pdfManager.authorizeDrive(self.view) { (success) -> Void in
            if success {
                self.buttonSubmit?.hidden = true
                self.buttonBack?.hidden = true
                if let _ = self.pdfView {
                    if self.pdfView!.isKindOfClass(UIScrollView) {
                        pdfManager.createPDFForScrollView(self.pdfView!, fileName: self.patient.selectedForms.first!.formTitle!.fileName, patient: self.patient, completionBlock: { (finished) -> Void in
                            if finished {
                                self.patient.selectedForms.removeFirst()
                                self.gotoNextForm(false)
                            } else {
                                self.buttonSubmit?.hidden = false
                                self.buttonBack?.hidden = false
                            }
                        })
                    }
                } else {
                    pdfManager.createPDFForView(self.view, fileName: self.patient.selectedForms.first!.formTitle!.fileName, patient: self.patient, completionBlock: { (finished) -> Void in
                        if finished {
                            self.patient.selectedForms.removeFirst()
                            self.gotoNextForm(false)
                        } else {
                            self.buttonSubmit?.hidden = false
                            self.buttonBack?.hidden = false
                        }
                    })
                }
            } else {
                self.buttonSubmit?.hidden = false
                self.buttonBack?.hidden = false
            }
        }
    }
    
    func gotoNextForm(isFromPatientInfoValue: Bool) {
        if isFromPreviousForm {
            if self.navigationController?.viewControllers.count > 2 {
                self.navigationController?.viewControllers.removeRange(2...(self.navigationController?.viewControllers.count)! - 2)
            }
        }

    
        let formNames = (patient.selectedForms as NSArray).valueForKey("formTitle") as! [String]
        if formNames.contains(kNewPatientSignInForm) {
            let patientSignIn = patientStoryboard.instantiateViewControllerWithIdentifier("PatientRegistrationStep1Vc") as! PatientRegistrationStep1Vc
            patientSignIn.patient = self.patient
            self.navigationController?.pushViewController(patientSignIn, animated: true)
        }else if formNames.contains(kMedicalHistoryForm){
            let medical = medicalStoryboard.instantiateViewControllerWithIdentifier("Medical1VC") as! MedicalHistory1ViewController
            medical.patient = self.patient
            self.navigationController?.pushViewController(medical, animated: true)
        }else if formNames.contains(kDentalHistory){
                let dental = medicalStoryboard.instantiateViewControllerWithIdentifier("Dental1VC") as! DentalHistory1ViewController
                dental.patient = self.patient
                self.navigationController?.pushViewController(dental, animated: true)
        }else if formNames.contains(kChildMedicalHistory){
            let medical = childMedicalStoryboard.instantiateViewControllerWithIdentifier("ChildMedical1VC") as! ChildMedicalHistory1ViewController
            medical.patient = self.patient
            self.navigationController?.pushViewController(medical, animated: true)
        }else if formNames.contains(kChildDentalHistory){
            let dental = childMedicalStoryboard.instantiateViewControllerWithIdentifier("ChildDental1VC") as! ChildDentalHistory1ViewController
            dental.patient = self.patient
            self.navigationController?.pushViewController(dental, animated: true)

        }else if formNames.contains(kInsuranceCard) {
            let cardCapture = mainStoryboard.instantiateViewControllerWithIdentifier("kCardImageCaptureVC") as! CardImageCaptureVC
            cardCapture.patient = self.patient
            cardCapture.isFromPatientInfo = isFromPatientInfoValue
            self.navigationController?.pushViewController(cardCapture, animated: true)
        } else if formNames.contains(kDrivingLicense) {
            let cardCapture = mainStoryboard.instantiateViewControllerWithIdentifier("kCardImageCaptureVC") as! CardImageCaptureVC
            cardCapture.patient = self.patient
            cardCapture.isDrivingLicense = true
            cardCapture.isFromPatientInfo = isFromPatientInfoValue
            self.navigationController?.pushViewController(cardCapture, animated: true)
        }else if formNames.contains(kNoticeOfPrivacy){
            let privacy = mainStoryboard.instantiateViewControllerWithIdentifier("Privacy1VC") as! PrivacyPractice1ViewController
            privacy.patient = self.patient
            self.navigationController?.pushViewController(privacy, animated: true)
         }else if formNames.contains(kInformedConsent){
            let informed = mainStoryboard.instantiateViewControllerWithIdentifier("InformedConsent1VC") as! InformedConsent1ViewController
            informed.patient = self.patient
            self.navigationController?.pushViewController(informed, animated: true)

         }else if formNames.contains(kFeedBack) {
            let feedBackVC = mainStoryboard.instantiateViewControllerWithIdentifier("kFeedBackInfoViewController") as! FeedBackInfoViewController
            feedBackVC.patient = self.patient
            self.navigationController?.pushViewController(feedBackVC, animated: true)
        }
        else {
            NSNotificationCenter.defaultCenter().postNotificationName(kFormsCompletedNotification, object: nil)
            self.navigationController?.popToRootViewControllerAnimated(true)
        }
        
    }
}

