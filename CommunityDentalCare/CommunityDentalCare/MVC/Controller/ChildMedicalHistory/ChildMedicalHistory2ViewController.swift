//
//  ChildMedicalHistory2ViewController.swift
//  CommunityDentalCare
//
//  Created by Bala Murugan on 9/29/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ChildMedicalHistory2ViewController: PDViewController {

    @IBOutlet var tableViewQuestions: UITableView!
    @IBOutlet var buttonVerified: UIButton!
    @IBOutlet var buttonNext: UIButton!
    @IBOutlet var buttonBack1: UIButton!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var labelHeading : UILabel!
    var selectedIndex: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        self.activityIndicator.stopAnimating()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onBackButton1Pressed (withSender sender: AnyObject) {
        if selectedIndex == 0 {
            self.navigationController?.popViewControllerAnimated(true)
        } else {
            buttonBack1.userInteractionEnabled = false
            buttonNext.userInteractionEnabled = false
            
            self.activityIndicator.startAnimating()
            let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.4 * Double(NSEC_PER_SEC)))
            dispatch_after(delayTime, dispatch_get_main_queue()) {
                self.activityIndicator.stopAnimating()
                self.selectedIndex = self.selectedIndex - 1
                self.labelHeading.text = self.selectedIndex == 0 ? "" : "Has your child ever had any of the following: *"
                self.tableViewQuestions.reloadData()
                self.buttonBack1.userInteractionEnabled = true
                self.buttonNext.userInteractionEnabled = true
            }
            
        }
    }
    
    @IBAction func buttonVerifiedAction(withSender sender: UIButton) {
        sender.selected = !sender.selected
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if buttonVerified.selected == false {
            let alert = Extention.alert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            if selectedIndex == 2 {
                let medical = childMedicalStoryboard.instantiateViewControllerWithIdentifier("ChildMedical3VC") as! ChildMedicalHistory3ViewController
                medical.patient = self.patient
                self.navigationController?.pushViewController(medical, animated: true)
                
            } else {
                buttonBack1.userInteractionEnabled = false
                buttonNext.userInteractionEnabled = false
                self.buttonVerified.selected = false
                self.activityIndicator.startAnimating()
                let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.4 * Double(NSEC_PER_SEC)))
                dispatch_after(delayTime, dispatch_get_main_queue()) {
                    self.activityIndicator.stopAnimating()
                    self.selectedIndex = self.selectedIndex + 1
                    self.labelHeading.text = self.selectedIndex == 0 ? "" : "Has your child ever had any of the following: *"
                    self.tableViewQuestions.reloadData()
                    self.buttonBack1.userInteractionEnabled = true
                    self.buttonNext.userInteractionEnabled = true
                }
            }
        }
    }
    
}

extension ChildMedicalHistory2ViewController : UITableViewDataSource,UITableViewDelegate {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patient.childMedicalHistory.medicalQuestions[selectedIndex].count
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! PatientInfoCell
        //cell.delegate = self
        cell.tag = indexPath.row
        cell.configCell(self.patient.childMedicalHistory.medicalQuestions[selectedIndex][indexPath.row])
        
        return cell
    }
}


