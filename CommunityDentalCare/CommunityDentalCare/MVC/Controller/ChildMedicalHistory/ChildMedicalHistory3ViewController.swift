//
//  ChildMedicalHistory3ViewController.swift
//  CommunityDentalCare
//
//  Created by Bala Murugan on 9/29/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ChildMedicalHistory3ViewController: PDViewController {

    @IBOutlet weak var textviewComments : UITextView!
//    @IBOutlet weak var signaturePatient : SignatureView!
//    @IBOutlet weak var labelDate : DateLabel!
    @IBOutlet var arrayAllergyButtons : [UIButton]!
    @IBOutlet var arrayTestButtons : [UIButton]!

    override func viewDidLoad() {
        super.viewDidLoad()

//        labelDate.todayDate = patient.dateToday
//        patient.childMedicalHistory.arrayAllergyTags = [Int]()
//        patient.childMedicalHistory.arrayTestTags = [Int]()
//        patient.childMedicalHistory.allergyOthers = "N/A"
        loadValues()


    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func saveValues (){
        patient.childMedicalHistory.patientComments = textviewComments.text! == "PLEASE TYPE HERE" ? "N/A" : textviewComments.text!

    }
    
    func loadValues()  {
        textviewComments.text = patient.childMedicalHistory.patientComments == "N/A" ? "PLEASE TYPE HERE" : patient.childMedicalHistory.patientComments
        if textviewComments.text == "PLEASE TYPE HERE"{
            textviewComments.textColor = UIColor.lightGrayColor()
        }else{
            textviewComments.textColor = UIColor.blackColor()
        }
        for btn in arrayAllergyButtons{
            btn.selected = patient.childMedicalHistory.arrayAllergyTags.contains(btn.tag)
        }
        for btn in arrayTestButtons{
            btn.selected = patient.childMedicalHistory.arrayTestTags.contains(btn.tag)
        }


    }
    
    @IBAction override func buttonActionBack(withSender sender : UIButton) {
        saveValues()
        super.buttonActionBack(withSender: sender)
    }

    @IBAction func onNextButtonPressed (withSender sender : UIButton){
//        if !signaturePatient.isSigned(){
//            let alert = Extention.alert("PLEASE SIGN THE FORM")
//            self.presentViewController(alert, animated: true, completion: nil)
//            
//        }else if !labelDate.dateTapped{
//            let alert = Extention.alert("PLEASE SELECT THE DATE")
//            self.presentViewController(alert, animated: true, completion: nil)
//            
//        }else{
            saveValues()
           // patient.childMedicalHistory.patientSignature = signaturePatient.signatureImage()
            let informed = mainStoryboard.instantiateViewControllerWithIdentifier("InformedConsent1VC") as! InformedConsent1ViewController
            informed.patient = self.patient
            self.navigationController?.pushViewController(informed, animated: true)

//            let medical = childMedicalStoryboard.instantiateViewControllerWithIdentifier("ChildMedicalFormVC") as! ChildMedicalHistoryFormViewController
//            medical.patient = self.patient
//            self.navigationController?.pushViewController(medical, animated: true)
            
//        }
        
    }


    @IBAction func allergyButtonAction (withSender sender : UIButton){
        sender.selected = !sender.selected
        if patient.childMedicalHistory.arrayAllergyTags.contains(sender.tag){
            patient.childMedicalHistory.arrayAllergyTags.removeAtIndex(patient.childMedicalHistory.arrayAllergyTags.indexOf(sender.tag)!)
        }else{
            patient.childMedicalHistory.arrayAllergyTags.append(sender.tag)
        }
        
        if sender.tag == 6 && sender.selected == true{
            PopupTextView.sharedInstance.showWithPlaceHolder("PLEASE SPECIFY", completion: { (textView, isEdited) in
                if isEdited{
                    self.patient.childMedicalHistory.allergyOthers = textView.text!
                    
                }else{
                    self.patient.childMedicalHistory.allergyOthers = "N/A"
                    sender.selected = false
                    self.patient.childMedicalHistory.arrayAllergyTags.removeAtIndex(self.patient.childMedicalHistory.arrayAllergyTags.indexOf(sender.tag)!)
                }
            })
            
        }else if sender.tag == 6 && sender.selected == false{
            self.patient.childMedicalHistory.allergyOthers = "N/A"
        }
        
    }
    
    @IBAction func testButtonAction (withSender sender : UIButton){
        sender.selected = !sender.selected
        if patient.childMedicalHistory.arrayTestTags.contains(sender.tag){
            patient.childMedicalHistory.arrayTestTags.removeAtIndex(patient.childMedicalHistory.arrayTestTags.indexOf(sender.tag)!)
        }else{
            patient.childMedicalHistory.arrayTestTags.append(sender.tag)
        }
        
    }

}

extension ChildMedicalHistory3ViewController : UITextViewDelegate {
    
    func textViewDidBeginEditing(textView: UITextView) {
        if textView.text == "PLEASE TYPE HERE" {
            textView.text = ""
            textView.textColor = UIColor.blackColor()
        }
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "PLEASE TYPE HERE"
            textView.textColor = UIColor.lightGrayColor()
        }
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}

