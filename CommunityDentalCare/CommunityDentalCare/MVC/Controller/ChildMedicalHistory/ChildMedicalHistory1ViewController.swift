//
//  ChildMedicalHistory1ViewController.swift
//  CommunityDentalCare
//
//  Created by Bala Murugan on 9/29/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ChildMedicalHistory1ViewController: PDViewController {

    @IBOutlet weak var textfieldPhysicianName : UITextField!
    @IBOutlet weak var textfieldAddress : UITextField!
    @IBOutlet weak var textfieldPhone : UITextField!
    @IBOutlet weak var textfieldLowBp : UITextField!
    @IBOutlet weak var textfieldHighBp : UITextField!

    @IBOutlet weak var textfieldPulse : UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        loadValues()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func saveValues (){
        patient.childMedicalHistory.physicianName = textfieldPhysicianName.isEmpty ? "N/A" : textfieldPhysicianName.text!
        patient.childMedicalHistory.physicianAddress = textfieldAddress.isEmpty ? "N/A" : textfieldAddress.text!
        patient.childMedicalHistory.PhysicianPhone = textfieldPhone.isEmpty ? "N/A" : textfieldPhone.text!
        patient.childMedicalHistory.bloodpressureLow = textfieldLowBp.isEmpty ? "" : textfieldLowBp.text!
        patient.childMedicalHistory.bloodpressureHigh = textfieldHighBp.isEmpty ? "" : textfieldHighBp.text!

        patient.childMedicalHistory.pulse = textfieldPulse.isEmpty ? "N/A" : textfieldPulse.text!
        
    }
    
    func loadValues()  {
        textfieldPhysicianName.setSavedText(patient.childMedicalHistory.physicianName)
        textfieldAddress.setSavedText(patient.childMedicalHistory.physicianAddress)
        textfieldPhone.setSavedText(patient.childMedicalHistory.PhysicianPhone)
        textfieldHighBp.setSavedText(patient.childMedicalHistory.bloodpressureHigh)
        textfieldLowBp.setSavedText(patient.childMedicalHistory.bloodpressureLow)
        textfieldPulse.setSavedText(patient.childMedicalHistory.pulse)
    }
    
    @IBAction override func buttonActionBack(withSender sender : UIButton) {
        saveValues()
        super.buttonActionBack(withSender: sender)
    }

    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
//        if textfieldAddress.isEmpty || textfieldPhysicianName.isEmpty {
//            let alert = Extention.alert("PLEASE ENTER ALL THE REQUIRED FIELDS")
//            self.presentViewController(alert, animated: true, completion: nil)
//            
//        }else
        if !textfieldPhone.isEmpty && !textfieldPhone.text!.isPhoneNumber{
            let alert = Extention.alert("PLEASE ENTER THE VALID PHONE NUMBER")
            self.presentViewController(alert, animated: true, completion: nil)
            
        }else{
            saveValues()
            let medical = childMedicalStoryboard.instantiateViewControllerWithIdentifier("ChildMedical2VC") as! ChildMedicalHistory2ViewController
            medical.patient = self.patient
            self.navigationController?.pushViewController(medical, animated: true)
            
        }
        
    }


}

extension ChildMedicalHistory1ViewController: UITextFieldDelegate {
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == textfieldPhone {
            return textField.formatPhoneNumber(range, string: string)
        }else if textField == textfieldLowBp || textField == textfieldPulse || textField == textfieldHighBp{
            return textField.formatNumbers(range, string: string, count: 3)
        }
        return true
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
}

