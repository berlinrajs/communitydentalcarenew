//
//  ChildMedicalHistoryFormViewController.swift
//  CommunityDentalCare
//
//  Created by Bala Murugan on 9/29/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ChildMedicalHistoryFormViewController: PDViewController {

    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelDateOfBirth : UILabel!
    @IBOutlet weak var labelBp : UILabel!
    @IBOutlet weak var labelPulse : UILabel!
    @IBOutlet weak var labelPhysicianName : UILabel!
    @IBOutlet weak var labelPhysicianTelephone : UILabel!
    @IBOutlet weak var labelPhysicianAddress : UILabel!
    @IBOutlet      var arrayRadioButtons : [RadioButton]!
    @IBOutlet      var arrayAllergyButtons : [UIButton]!
    @IBOutlet weak var labelAllergyOthers : UILabel!
    @IBOutlet      var arrayTestButtons : [UIButton]!
    @IBOutlet      var arrayMedicalButtons1 : [UIButton]!
    @IBOutlet      var arrayMedicalButtons2 : [UIButton]!
    @IBOutlet weak var textviewComments : UITextView!
    @IBOutlet      var labelOfficeComments : [UILabel]!
    @IBOutlet weak var signaturePatient : UIImageView!
    @IBOutlet weak var signatureOffice : UIImageView!
    @IBOutlet weak var labelDate1 : UILabel!
    @IBOutlet weak var labelDate2 : UILabel!
    @IBOutlet weak var radioAllergy : RadioButton!
    @IBOutlet weak var radioTest : RadioButton!
    
    
    @IBOutlet weak var labelPatientName1 : UILabel!
    @IBOutlet weak var labelDateOfBirth1 : UILabel!
    @IBOutlet var buttonTechniques : [RadioButton]!
    @IBOutlet weak var signatureParent : UIImageView!
    @IBOutlet weak var signatureWitness : UIImageView!
    @IBOutlet weak var signatureDoctor : UIImageView!
    @IBOutlet weak var labelDate4 : UILabel!
    @IBOutlet weak var labelDate5 : UILabel!
    @IBOutlet weak var labelDate3 : UILabel!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadData(){
        labelPatientName.text = patient.fullName
        labelDateOfBirth.text = patient.dateOfBirth
        labelBp.text = patient.childMedicalHistory.bloodpressureLow == "" && patient.childMedicalHistory.bloodpressureHigh == "" ? "N/A" : patient.childMedicalHistory.bloodpressureLow + " - " + patient.childMedicalHistory.bloodpressureHigh
        labelPulse.text = patient.childMedicalHistory.pulse
        labelPhysicianName.text = patient.childMedicalHistory.physicianName
        labelPhysicianTelephone.text = patient.childMedicalHistory.PhysicianPhone
        labelPhysicianAddress.text = patient.childMedicalHistory.physicianAddress
        for btn in arrayRadioButtons{
            btn.selected = patient.childMedicalHistory.medicalQuestions[0][btn.tag].selectedOption == true
        }
        for btn in arrayAllergyButtons{
            btn.selected = patient.childMedicalHistory.arrayAllergyTags.contains(btn.tag)
        }
        labelAllergyOthers.text = patient.childMedicalHistory.allergyOthers
        for btn in arrayTestButtons{
            btn.selected = patient.childMedicalHistory.arrayTestTags.contains(btn.tag)

        }
        for btn in arrayMedicalButtons1{
            btn.selected = patient.childMedicalHistory.medicalQuestions[1][btn.tag].selectedOption == true
        }
        for btn in arrayMedicalButtons2{
            btn.selected = patient.childMedicalHistory.medicalQuestions[1][btn.tag].selectedOption == true
        }

        textviewComments.text = patient.childMedicalHistory.patientComments
        patient.childMedicalHistory.childMedicalHistoryOfficeComments.setTextForArrayOfLabels(labelOfficeComments)
        signaturePatient.image = patient.informedSignatureParent
        signatureOffice.image = patient.childMedicalHistory.childMedicalHistoryOfficeSign
        labelDate1.text = patient.dateToday
        labelDate2.text = patient.childMedicalHistory.childMedicalHistoryOfficeComments == "" ? "" : patient.dateToday
        radioAllergy.setSelected(patient.childMedicalHistory.arrayAllergyTags.count > 0)
        radioTest.setSelected(patient.childMedicalHistory.arrayTestTags.count > 0)
        
        
        labelPatientName1.text = patient.fullName
        labelDateOfBirth1.text = patient.dateOfBirth
        signatureParent.image = patient.informedSignatureParent
        signatureWitness.image = patient.informedSignatureWitness
        signatureDoctor.image = patient.informedSignatureDoctor
        for btn in buttonTechniques{
            btn.selected = patient.informedConsentTechniqueTag.contains(btn.tag)
        }
        labelDate5.text = patient.dateToday
        labelDate4.text = patient.dateToday
        labelDate3.text = patient.dateToday
        


    }


}
