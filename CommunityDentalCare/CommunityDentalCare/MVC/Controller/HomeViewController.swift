//
//  HomeViewController.swift
//  Advanced Aesthetic Dentistry
//
//  Created by Leojin Bose on 17/02/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

public let screenSize = UIScreen.mainScreen().bounds



class HomeViewController: PDViewController {

    
    @IBOutlet weak var textFieldToothNumbers: PDTextField!
    @IBOutlet weak var labelAlertHeader: UILabel!
    @IBOutlet weak var labelAlertFooter: UILabel!
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet var viewToothNumbers: PDView!
    
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var tableViewForms: UITableView!
    @IBOutlet weak var viewAlert: PDView!
    @IBOutlet weak var buttonLogout: UIButton!
    
    @IBOutlet weak var labelVersion: UILabel!
    
    var selectedForms : [Forms]! = [Forms]()
    var formList : [Forms]! = [Forms]()
    var consentIndex : Int = 9
    
    var medicalHistoryOfficeComments : String = ""
    var medicalHistoryOfficeSign : UIImage = UIImage()
    
    var dentalHistoryOfficeComments : String = ""
    var dentalHistoryOfficeSign : UIImage = UIImage()
    
    var childMedicalHistoryOfficeComments : String = ""
    var childMedicalHistoryOfficeSign : UIImage = UIImage()

    var childDentalHistoryOfficeComments : String = ""
    var childDentalHistoryOfficeSign : UIImage = UIImage()


    override func viewDidLoad() {
        super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(HomeViewController.showAlert as (HomeViewController) -> () -> ()), name: kFormsCompletedNotification, object: nil)

        if let text = NSBundle.mainBundle().infoDictionary?[kCFBundleVersionKey as String] as? String {
            labelVersion.text = text
        }
        // Do any additional setup after loading the view, typically from a nib.
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(dateChangedNotification), name: kDateChangedNotification, object: nil)
        self.dateChangedNotification()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    func dateChangedNotification() {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy"
        labelDate.text = dateFormatter.stringFromDate(NSDate()).uppercaseString
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        Forms.getAllForms { (isConnectionFailed, forms) -> Void in
            self.formList = forms
            self.tableViewForms.reloadData()
            if isConnectionFailed == true {
                let alertController = UIAlertController(title: "Community Dental Care", message: "Your device is not connected to internet. Please go to settings to connect.", preferredStyle: UIAlertControllerStyle.Alert)
                let alertOkAction = UIAlertAction(title: "Settings", style: UIAlertActionStyle.Destructive) { (action) -> Void in
                    let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
                    if let url = settingsUrl {
                        UIApplication.sharedApplication().openURL(url)
                    }
                }
                let alertCancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Destructive) { (action) -> Void in
                    
                }
                alertController.addAction(alertOkAction)
                alertController.addAction(alertCancelAction)
                self.presentViewController(alertController, animated: true, completion: nil)
            }
        }


    }
    
    @IBAction func buttonActionNext(sender: AnyObject) {
        selectedForms.removeAll()
        for (_, form) in formList.enumerate() {
            if form.isSelected == true {
                if form.formTitle == kConsentForms  {
                    for subForm in form.subForms {
                        if subForm.isSelected == true {
                            selectedForms.append(subForm)
                        }
                    }
                } else {
                    selectedForms.append(form)
                }
            }
        }
        self.view.endEditing(true)
        if selectedForms.count > 0 {
            selectedForms.sortInPlace({ (formObj1, formObj2) -> Bool in
                return formObj1.index < formObj2.index
            })
        }
        
        let patient = PDPatient(forms: selectedForms)
        patient.dateToday = labelDate.text
        patient.medicalHistory.medicalHistoryOfficeComments = self.medicalHistoryOfficeComments
        patient.medicalHistory.medicalHistoryOfficeSign = self.medicalHistoryOfficeSign
        
        patient.childMedicalHistory.childMedicalHistoryOfficeComments = self.childMedicalHistoryOfficeComments
        patient.childMedicalHistory.childMedicalHistoryOfficeSign = self.childMedicalHistoryOfficeSign

        patient.dentalHistory.dentalHistoryOfficeComments = self.dentalHistoryOfficeComments
        patient.dentalHistory.dentalHistoryOfficeSign = self.dentalHistoryOfficeSign
        
        patient.childDentalHistory.childDentalHistoryOfficeComments = self.childDentalHistoryOfficeComments
        patient.childDentalHistory.childDentalHistoryOfficeSign = self.childDentalHistoryOfficeSign


        
        let formNames = (patient.selectedForms as NSArray).valueForKey("formTitle") as! [String]
        
        if formNames.contains(kVisitorCheckInForm) && selectedForms.count > 0{
            
            let patientInfoVC = self.storyboard?.instantiateViewControllerWithIdentifier("kVisitorVC") as! VisitorCheckinVC
            patientInfoVC.patient = patient
            self.navigationController?.pushViewController(patientInfoVC, animated: true)
            
        }else if selectedForms.count == 1 && selectedForms.first!.formTitle == kFeedBack {
            
            let patientInfoVC = self.storyboard?.instantiateViewControllerWithIdentifier("kFeedBackInfoViewController") as! FeedBackInfoViewController
            patientInfoVC.patient = patient
            self.navigationController?.pushViewController(patientInfoVC, animated: true)
        }else if selectedForms.count > 0 {
            
            let patientInfoVC = self.storyboard?.instantiateViewControllerWithIdentifier("kPatientInfoVC") as! PatientInfoViewController
            patientInfoVC.patient = patient
            self.navigationController?.pushViewController(patientInfoVC, animated: true)
        } else {
            let alert = Extention.alert("PLEASE SELECT ANY FORM")
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    @IBAction func buttonActionLogout(sender: AnyObject) {
//        GTMOAuth2ViewControllerTouch.removeAuthFromKeychainForName(kKeychainItemName)
//        buttonLogout.hidden = true
    }
    
    

    
    @IBAction func buttonActionDone(sender: AnyObject) {
        textFieldToothNumbers.resignFirstResponder()
        self.viewToothNumbers.removeFromSuperview()
        self.viewShadow.hidden = true
        let form =  textFieldToothNumbers.tag <= consentIndex ? formList[textFieldToothNumbers.tag] : formList[consentIndex].subForms[textFieldToothNumbers.tag - (consentIndex + 1)]
        if !textFieldToothNumbers.isEmpty || textFieldToothNumbers.tag == 12 {
            form.isSelected = true
            form.toothNumbers = textFieldToothNumbers.text
        } else {
            form.isSelected = false
        }
        self.tableViewForms.reloadData()

    }
    
    func showAlert() {
        viewAlert.hidden = false
        viewShadow.hidden = false
    }
    
    @IBAction func buttonActionOk(sender: AnyObject) {
        
        viewAlert.hidden = true
        viewShadow.hidden = true
    }
    
    
    func showPopup() {
        textFieldToothNumbers.text = ""
        self.viewToothNumbers.frame = CGRectMake(0, 0, 512.0, 250.0)
        self.viewToothNumbers.center = self.view.center
        self.viewShadow.addSubview(self.viewToothNumbers)
        self.viewToothNumbers.transform = CGAffineTransformMakeScale(0.1, 0.1)
        self.viewShadow.hidden = false
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.viewToothNumbers.transform = CGAffineTransformIdentity
        UIView.commitAnimations()
    }
    


}

extension HomeViewController : UITextFieldDelegate {
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
     
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {

                if string.lengthOfBytesUsingEncoding(NSUTF8StringEncoding) == 0 {
            return true
        }
        if string.rangeOfCharacterFromSet(NSCharacterSet(charactersInString: "01234567890,").invertedSet)?.last != nil {
            return false
        }
        
        let newRange = textField.text!.startIndex.advancedBy(range.location)..<textField.text!.startIndex.advancedBy(range.location + range.length)
        let textFieldString = textField.text!.stringByReplacingCharactersInRange(newRange, withString: string)
        let textString = textFieldString.componentsSeparatedByString(",")
        
        if textFieldString.characters.count > 2 {
            let lastString = textFieldString.substringFromIndex(textFieldString.startIndex.advancedBy(textFieldString.characters.count - 1))
            let lastTwoStrings = textFieldString.substringFromIndex(textFieldString.startIndex.advancedBy(textFieldString.characters.count - 2))
            let lastThreeStrings = textFieldString.substringFromIndex(textFieldString.startIndex.advancedBy(textFieldString.characters.count - 3))
            
            if lastTwoStrings == ",," {
                return false
            }
            if lastString == "," && lastThreeStrings.componentsSeparatedByString(",").count == 3 {
                let requiredString = textFieldString.substringToIndex(textFieldString.startIndex.advancedBy(textFieldString.characters.count - 2)) + "0" + lastTwoStrings
                textField.text = requiredString
                return false
            }
            
        } else {
            if textFieldString.characters.count == 2 {
                let lastString = textFieldString.substringFromIndex(textFieldString.startIndex.advancedBy(textFieldString.characters.count - 1))
                if lastString == "," {
                    textField.text = "0" + textFieldString
                    return false
                }
                
            }
            if textFieldString == "," {
                return false
            }
        }
        
        
        
        for text in textString {
            if text == "0" {
                return true
            }
            if text == "00" {
                return false
            }
            if Int(text) > 35 {
                return false
            }
        }
        return true
    }
}


extension HomeViewController : UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == consentIndex {
            let subForms = formList[consentIndex].subForms
            for subFrom in subForms {
                subFrom.isSelected = false
            }
            let form = self.formList[consentIndex]
            form.isSelected = !form.isSelected
            var indexPaths : [NSIndexPath] = [NSIndexPath]()
            for (idx, _) in form.subForms.enumerate() {
                let indexPath = NSIndexPath(forRow: consentIndex + 1 + idx, inSection: 0)
                indexPaths.append(indexPath)
            }
            if form.isSelected == true {
                tableView.beginUpdates()
                tableView.insertRowsAtIndexPaths(indexPaths, withRowAnimation: .Bottom)
                tableView.endUpdates()
                let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.2 * Double(NSEC_PER_SEC)))
                dispatch_after(delayTime, dispatch_get_main_queue()) {
                    if form.isSelected == true {
                        tableView.scrollToRowAtIndexPath(indexPaths.last!, atScrollPosition: .Bottom, animated: true)
                    }
                }
            } else {
                tableView.beginUpdates()
                tableView.deleteRowsAtIndexPaths(indexPaths, withRowAnimation: .Bottom)
                tableView.endUpdates()
            }
            tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .None)
            return
        }
       // let formsCount = formList[5].isSelected == true ? formList.count + formList[5].subForms.count  : formList.count
        var form : Forms!
        if (indexPath.row <= consentIndex) {
            form = formList[indexPath.row]
        } else if formList[consentIndex].isSelected && indexPath.row <= consentIndex + formList[consentIndex].subForms.count {
            form = formList[consentIndex].subForms[indexPath.row - (consentIndex + 1)]
        } else {
            form = formList.last
        }
        
        if form.isToothNumberRequired == true && !form.isSelected {
            textFieldToothNumbers.tag = indexPath.row
            self.showPopup()

        } else {
            form.isSelected = !form.isSelected
        }
        tableView.reloadData()
//        ///MEDICAL HISTORY
//        if indexPath.row == 2 && self.formList[2].isSelected == true{
//            MedicalHistoryAlert.sharedInstance.showPopUp(self.view, date: labelDate.text!, completion: { (comments, sign) in
//                self.medicalHistoryOfficeSign = sign
//                self.medicalHistoryOfficeComments = comments
//                }, showAlert: { (alertMessage) in
//                    let alert = Extention.alert(alertMessage)
//                    self.presentViewController(alert, animated: true, completion: nil)
//            })
//        }
//        ///DENTAL HISTORY
//        if indexPath.row == 3 && self.formList[3].isSelected == true{
//            MedicalHistoryAlert.sharedInstance.showPopUp(self.view, date: labelDate.text!, completion: { (comments, sign) in
//                self.dentalHistoryOfficeSign = sign
//                self.dentalHistoryOfficeComments = comments
//                }, showAlert: { (alertMessage) in
//                    let alert = Extention.alert(alertMessage)
//                    self.presentViewController(alert, animated: true, completion: nil)
//            })
//            
//        }
//        ///CHILD MEDICAL HISTORY
//        if indexPath.row == 4 && self.formList[4].isSelected == true{
//            MedicalHistoryAlert.sharedInstance.showPopUp(self.view, date: labelDate.text!, completion: { (comments, sign) in
//                self.childMedicalHistoryOfficeSign = sign
//                self.childMedicalHistoryOfficeComments = comments
//                }, showAlert: { (alertMessage) in
//                    let alert = Extention.alert(alertMessage)
//                    self.presentViewController(alert, animated: true, completion: nil)
//            })
//            
//        }
//
//        ///CHILD DENTAL HISTORY
//        if indexPath.row == 5 && self.formList[5].isSelected == true{
//            MedicalHistoryAlert.sharedInstance.showPopUp(self.view, date: labelDate.text!, completion: { (comments, sign) in
//                self.childDentalHistoryOfficeSign = sign
//                self.childDentalHistoryOfficeComments = comments
//                }, showAlert: { (alertMessage) in
//                    let alert = Extention.alert(alertMessage)
//                    self.presentViewController(alert, animated: true, completion: nil)
//            })
//            
//        }

        


        
    }
}

extension HomeViewController : UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       // if formList.count > 0 {
          //  let subForms = formList[consentIndex].subForms
//            return formList[consentIndex].isSelected == true ? formList.count + subForms.count  : formList.count
//        } else {
            return formList.count
//        }
        
    }
//    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
//        if(indexPath.row == 7){
//            return 60
//        }
//        else if(indexPath.row == 8){
//            return 60
//        } else if (indexPath.row == 10){
//            return 60
//        }else if (indexPath.row == 13){
//            return 60
//        }
//        else if (indexPath.row == 14){
//            return 60
//        }
//        else{
//            return 44 //a default size if the cell index path is anything other than the 1st or second row.
//        }
//
//    }

    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if (indexPath.row <= consentIndex) {
            let cell = tableView.dequeueReusableCellWithIdentifier("cellMainForm", forIndexPath: indexPath) as! FormsTableViewCell
            let form = formList[indexPath.row]
            cell.labelFormName.text = form.formTitle
            cell.imageViewCheckMark.hidden = !form.isSelected
            cell.backgroundColor = UIColor.clearColor()
            cell.contentView.backgroundColor = UIColor.clearColor()
            return cell
        } else if formList[consentIndex].isSelected && indexPath.row <= consentIndex + formList[consentIndex].subForms.count {
            let cell = tableView.dequeueReusableCellWithIdentifier("cellSubForm", forIndexPath: indexPath) as! FormsTableViewCell
            let form = formList[consentIndex].subForms[indexPath.row - (consentIndex + 1)]
            cell.labelFormName.text = form.formTitle
            cell.imageViewCheckMark.hidden = !form.isSelected
            cell.backgroundColor = UIColor.clearColor()
            cell.contentView.backgroundColor = UIColor.clearColor()
            return cell
        } else {
            let cell = tableView.dequeueReusableCellWithIdentifier("cellMainForm", forIndexPath: indexPath) as! FormsTableViewCell
            let form = formList[consentIndex + 1]
            cell.labelFormName.text = form.formTitle
            cell.imageViewCheckMark.hidden = !form.isSelected
            cell.backgroundColor = UIColor.clearColor()
            cell.contentView.backgroundColor = UIColor.clearColor()
            return cell
        }
        
    }
}


