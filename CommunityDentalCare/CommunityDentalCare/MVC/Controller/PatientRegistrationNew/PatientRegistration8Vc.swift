//
//  PatientRegistration8Vc.swift
//  CommunityDentalCare
//
//  Created by SRS Web Solutions on 28/09/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PatientRegistration8Vc: PDViewController {

    @IBOutlet var textFieldPolicyholderName: MCTextField!
    @IBOutlet var textFieldInsuredName: MCTextField!
    @IBOutlet var textFieldInsuredAddress: MCTextField!
    @IBOutlet var textFieldGroupId: MCTextField!
    @IBOutlet var textFieldPolicyId: MCTextField!
    @IBOutlet var textFieldPhoneNumber: MCTextField!
    @IBOutlet var textFieldDateOfBirth: MCTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textFieldPhoneNumber.textFormat = .Phone
        textFieldDateOfBirth.textFormat = .DateIn1980
        
        //Autofill
        
        textFieldPolicyholderName.text = patient.secondaryInsurance.policyholderName
        textFieldInsuredName.text = patient.secondaryInsurance.insuredPlanName
        textFieldInsuredAddress.text = patient.secondaryInsurance.address
        textFieldGroupId.text = patient.secondaryInsurance.groupId
        textFieldPolicyId.text = patient.secondaryInsurance.policyId
        textFieldPhoneNumber.text = patient.secondaryInsurance.phone
        textFieldDateOfBirth.text = patient.secondaryInsurance.dateOfBirth 
        
        // Do any additional setup after loading the view.
    }
    
    func saveValues () {
        
        patient.secondaryInsurance.policyholderName = textFieldPolicyholderName.text!
        patient.secondaryInsurance.insuredPlanName = textFieldInsuredName.text!
        patient.secondaryInsurance.address = textFieldInsuredAddress.text!
        patient.secondaryInsurance.groupId = textFieldGroupId.text!
        patient.secondaryInsurance.policyId = textFieldPolicyId.text!
        patient.secondaryInsurance.phone = textFieldPhoneNumber.text!
        patient.secondaryInsurance.dateOfBirth = textFieldDateOfBirth.text!
    }
    
    
    
    @IBAction override func buttonActionBack(withSender sender: UIButton) {
        saveValues()
        super.buttonActionBack(withSender: sender)
        
        
    }
    
    
    
    
    
    
    @IBAction func ButtonActionNext(withsender sender: AnyObject) {
        if !textFieldPhoneNumber.isEmpty && !textFieldPhoneNumber.text!.isPhoneNumber {
            self.showAlert("PLEASE ENTER A VALID PHONE NUMBER")
        }else {
        
            let step3VC = self.storyboard?.instantiateViewControllerWithIdentifier("PatientRegistration9Vc") as! PatientRegistration9Vc
            patient.secondaryInsurance.policyholderName = textFieldPolicyholderName.text!
            patient.secondaryInsurance.insuredPlanName = textFieldInsuredName.text!
            patient.secondaryInsurance.address = textFieldInsuredAddress.text!
            patient.secondaryInsurance.groupId = textFieldGroupId.text!
            patient.secondaryInsurance.policyId = textFieldPolicyId.text!
            patient.secondaryInsurance.phone = textFieldPhoneNumber.text!
            patient.secondaryInsurance.dateOfBirth = textFieldDateOfBirth.text!
            step3VC.patient = self.patient
            self.navigationController?.pushViewController(step3VC, animated: true)
            
            
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
}
