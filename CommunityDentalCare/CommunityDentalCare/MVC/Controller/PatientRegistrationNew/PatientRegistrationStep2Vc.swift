//
//  PatientRegistrationStep2Vc.swift
//  CommunityDentalCare
//
//  Created by SRS Web Solutions on 27/09/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PatientRegistrationStep2Vc: PDViewController {
    
    @IBOutlet weak var textFieldAddress: MCTextField!
    @IBOutlet weak var textFieldCity: MCTextField!
    @IBOutlet weak var textFieldState: MCTextField!
    @IBOutlet weak var textFieldZip: MCTextField!
    @IBOutlet weak var textFieldSocialSecurityNumber: MCTextField!
    
    @IBOutlet weak var dropDownMaritalStatus: BRDropDown!
    @IBOutlet weak var radioGender: RadioButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textFieldState.textFormat = .State
        textFieldZip.textFormat = .Zipcode
        //        textFieldSocialSecurityNumber.textFormat = .SocialSecurity
        
        dropDownMaritalStatus.items = ["CHILD", "SINGLE", "PARTNERED", "MARRIED", "SEPERATED", "DIVORCED", "WIDOWED"]
        dropDownMaritalStatus.placeholder = "-- MARITAL STATUS *--"
        // Do any additional setup after loading the view.
        //Autofill
        
        textFieldAddress.text! = patient.address
        textFieldCity.text! = patient.city
        textFieldState.text! = patient.state
        textFieldZip.text! =  patient.zip
        
        if patient.maritalStatusText != nil{
            dropDownMaritalStatus.setSelectedOption = patient.maritalStatusText
        }
       
        if patient.gender != nil {
            radioGender.setSelectedWithTag(patient.gender)
 
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func saveValues () {
        patient.address = textFieldAddress.text!
        patient.city = textFieldCity.text!
        patient.state = textFieldState.text!
        patient.zip = textFieldZip.text!
//        patient.maritalStatus = dropDownMaritalStatus.selectedIndex
        if radioGender.selected {
        patient.gender = radioGender.selectedButton.tag
        }
        
        if dropDownMaritalStatus.selectedIndex != 0 {
            patient.maritalStatusText =  dropDownMaritalStatus.selectedOption == nil ? "-- MARITAL STATUS *--" : dropDownMaritalStatus.selectedOption!
        }
    }
    
    
    
    @IBAction override func buttonActionBack(withSender sender: UIButton) {
        saveValues()
        super.buttonActionBack(withSender: sender)
        
        
    }

    
    
    
    
    @IBAction func buttonNextAction() {
        self.view.endEditing(true)
        dropDownMaritalStatus.selected = false
        if dropDownMaritalStatus.selectedIndex == 0 {
            self.showAlert("PLEASE SELECT MARITAL STATUS")
        } else if textFieldAddress.isEmpty {
            self.showAlert("PLEASE ENTER A VALID ADDRESS")
        }else if textFieldCity.isEmpty{
            self.showAlert("PLEASE ENTER A VALID CITY")
        }else if textFieldState.isEmpty{
            self.showAlert("PLEASE ENTER A VALID STATE")
        } else if textFieldZip.isEmpty || !textFieldZip.text!.isZipCode {
            self.showAlert("PLEASE ENTER A VALID ZIPCODE")
        } else if radioGender.selectedButton == nil {
            self.showAlert("PLEASE SELECT YOUR GENDER")
        } else {
            patient.address = textFieldAddress.text!
            patient.city = textFieldCity.text!
            patient.state = textFieldState.text!
            patient.zip = textFieldZip.text!//
            patient.maritalStatus = dropDownMaritalStatus.selectedIndex
            patient.gender = radioGender.selectedButton.tag
            patient.maritalStatusText = dropDownMaritalStatus.selectedOption
            
            let step2ContactVC = patientStoryboard.instantiateViewControllerWithIdentifier("PatientRegistrationStep3Vc") as! PatientRegistrationStep3Vc
            step2ContactVC.patient = self.patient
            self.navigationController?.pushViewController(step2ContactVC, animated: true)
        }
    }
    
}
