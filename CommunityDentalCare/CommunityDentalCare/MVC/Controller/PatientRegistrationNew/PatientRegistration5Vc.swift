//
//  PatientRegistration5Vc.swift
//  CommunityDentalCare
//
//  Created by SRS Web Solutions on 28/09/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PatientRegistration5Vc: PDViewController {

    
    @IBOutlet var dropDownRelation: BRDropDown!
    @IBOutlet weak var textFieldName: MCTextField!
    @IBOutlet var textFieldLastName: MCTextField!
    @IBOutlet weak var textFieldNumber: MCTextField!
    
    var otherRelation: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dropDownRelation.items = ["SPOUSE", "CHILD", "GUARDIAN", "OTHER"]
        dropDownRelation.placeholder = "-- RELATIONSHIP TO PATIENT --"
        dropDownRelation.delegate = self
        textFieldNumber.textFormat = .Phone
        
        //Autofill
        textFieldName.text = patient.emergencyFirstName
        textFieldLastName.text = patient.emergencyLastName
        textFieldNumber.text = patient.emergencyPhone
        
        if patient.emerRelation != nil {
            dropDownRelation.selectedIndex = patient.emerRelation
        }
        
        self.otherRelation = patient.emergencyRelationOther
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func saveValues () {
        patient.emergencyFirstName = textFieldName.text!
        patient.emergencyLastName = textFieldLastName.text!
        patient.emergencyPhone = textFieldNumber.text!
        patient.emergencyRelation = dropDownRelation.selectedOption == nil ? "" : dropDownRelation.selectedOption!
        patient.emerRelation = dropDownRelation.selectedIndex
        patient.emergencyRelationOther = self.otherRelation

    }
    
    
    
    @IBAction override func buttonActionBack(withSender sender: UIButton) {
        saveValues()
        super.buttonActionBack(withSender: sender)
        
        
    }

    
    
    
    
    
    @IBAction func buttonNextAction() {
        if !textFieldNumber.isEmpty && !textFieldNumber.text!.isPhoneNumber {
            self.showAlert("PLEASE ENTER A VALID PHONE NUMBER")
        } else {
            patient.emergencyFirstName = textFieldName.text!
            patient.emergencyLastName = textFieldLastName.text!
            patient.emergencyPhone = textFieldNumber.text!
            patient.emergencyRelation = dropDownRelation.selectedOption == nil ? "" : dropDownRelation.selectedOption!
            patient.emerRelation = dropDownRelation.selectedIndex
            
            patient.emergencyRelationOther = self.otherRelation
            
            let step3VC = self.storyboard?.instantiateViewControllerWithIdentifier("PatientRegistration6Vc") as! PatientRegistration6Vc
            step3VC.patient = self.patient
            self.navigationController?.pushViewController(step3VC, animated: true)
        }
    }
}
extension PatientRegistration5Vc: BRDropDownDelegate {
    func dropDown(dropDown: BRDropDown, selectedAtIndex index: Int, selectedOption option: String?) {
        if index == 4 {
            PopupTextField.sharedInstance.showWithPlaceHolder("RELATIONSHIP TO PATIENT", keyboardType: UIKeyboardType.Default, textFormat: TextFormat.Default, completion: { (textField, isEdited) in
                if !textField.isEmpty {
//                    self.dropDownRelation.reset()
                    self.otherRelation = textField.text!
                } else {
                    self.otherRelation = ""
                    self.dropDownRelation.reset()
                }
                
            })
            
            
            

//            PopupTextField.popUpView().showWithTitle("PLEASE SPECIFY THE RELATION", placeHolder: "RELATIONSHIP TO PATIENT *", textFormat: TextFormat.Default, completion: { (textField, isEdited) in
//                if textField.isEmpty {
//                    self.dropDownRelation.reset()
//                    self.otherRelation = textField.text!
//                } else {
//                    self.otherRelation = ""
//                }
//            })
        } else {
            self.otherRelation = option == nil ? "" : option!.uppercaseString
        }
    }
}

