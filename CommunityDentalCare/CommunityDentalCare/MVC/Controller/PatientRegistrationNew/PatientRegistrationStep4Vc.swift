//
//  PatientRegistrationStep4Vc.swift
//  CommunityDentalCare
//
//  Created by SRS Web Solutions on 27/09/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PatientRegistrationStep4Vc: PDViewController {
    
    @IBOutlet var dropDownLanguage: BRDropDown!
    @IBOutlet var dropdownRace: BRDropDown!
    @IBOutlet var dropDownEthicity: BRDropDown!
    @IBOutlet var dropDownReferredBy: BRDropDown!
    
    
    var otherLanguage: String = ""
    var otherRace: String = ""
    var otherEthicity: String = ""
    var otherReferral: String = ""

    
    override func viewDidLoad() {
        super.viewDidLoad()
        dropDownLanguage.items = ["ENGLISH", "OTHER"]
        dropDownLanguage.placeholder = "-- LANGUAGE *--"
        dropdownRace.items = ["ASIAN", "AFRICANAMERICAN","CAUCASIAN","NATIVEAMERICAN", "HAWAIIAN", "PACIFICISLANDER", "OTHER"]
        dropdownRace.placeholder = "-- RACE *--"
        dropDownEthicity.items = ["HISPANIC", "HMONG", "KAREN", "SOMALI", "OTHER"]
        dropDownEthicity.placeholder = "-- ETHNICITY *--"
        dropDownReferredBy.items = ["FAMILY", "FRIEND","INTERPRETER","INSURANCE PLAN", "COUNTY WORKER", "DENTAL OFFICE", "HOSPITAL", "SCHOOL", "PHONE BOOK", "WALK-IN", "INTERNET", "FLYER", "COMMUNITY EVENT", "PORTICO HEALTHNET", "OTHER"]
        dropDownReferredBy.placeholder = "-- REFERRED BY *--"
        
        dropDownLanguage.delegate = self
        dropdownRace.delegate = self
        dropDownEthicity.delegate = self
        dropDownReferredBy.delegate = self
        
        
        //Autofill
        
        if patient.arrayLanguage != nil{
            dropDownLanguage.setSelectedOption = patient.arrayLanguage
        }
        if patient.arrayrace != nil{
            dropdownRace.setSelectedOption = patient.arrayrace
        }
        if patient.arrayethnicity != nil{
            dropDownEthicity.setSelectedOption = patient.arrayethnicity
        }
        if patient.arrayreferral != nil{
            dropDownReferredBy.setSelectedOption = patient.arrayreferral
        }
        
        
        self.otherLanguage = patient.languageOther
        self.otherRace = patient.raceOther
        self.otherEthicity = patient.ethnicityOther
        self.otherReferral = patient.referredOther

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func saveValues () {
        
        if dropDownLanguage.selectedIndex != 0 {
            patient.arrayLanguage =  dropDownLanguage.selectedOption == nil ? "-- LANGUAGE *--" : dropDownLanguage.selectedOption!
        }

        if dropdownRace.selectedIndex != 0 {
           patient.arrayrace =  dropdownRace.selectedOption == nil ? "-- RACE *--" : dropdownRace.selectedOption!
        }
        if dropDownEthicity.selectedIndex != 0 {
            patient.arrayethnicity =  dropDownEthicity.selectedOption == nil ? "-- ETHNICITY *--" : dropDownEthicity.selectedOption!
        }
        if dropDownReferredBy.selectedIndex != 0 {
            patient.arrayreferral =  dropDownReferredBy.selectedOption == nil ? "-- REFERRED BY *--" : dropDownReferredBy.selectedOption!
        }
        
       
        
        
        
//        if dropDownLanguage.
//        patient.languageDropDown = dropDownLanguage.selectedIndex
//        patient.raceDropDown = dropdownRace.selectedIndex
//        patient.ethnicityDropDown = dropDownEthicity.selectedIndex
//        patient.referralDropDown = dropDownReferredBy.selectedIndex
//        patient.languageOther = self.otherLanguage
//        patient.raceOther = self.otherRace
//        patient.ethnicityOther = self.otherEthicity
//        patient.referredOther = self.otherReferral
//        
//        patient.arrayLanguage = dropDownLanguage.selectedOption
//        patient.arrayrace = dropdownRace.selectedOption
//        patient.arrayethnicity = dropDownEthicity.selectedOption
//        patient.arrayreferral = dropDownReferredBy.selectedOption
        
    }
    
    
    
    @IBAction override func buttonActionBack(withSender sender: UIButton) {
        saveValues()
        super.buttonActionBack(withSender: sender)
      
    }
    

    
    
    
    @IBAction func buttonNextAction() {
        self.view.endEditing(true)
        dropDownLanguage.selected = false
        dropdownRace.selected = false
        dropDownEthicity.selected = false
        dropDownReferredBy.selected = false
        if dropDownLanguage.selectedIndex == 0 {
            self.showAlert("PLEASE SELECT THE LANGUAGE")
        } else if dropdownRace.selectedIndex == 0{
               self.showAlert("PLEASE SELECT THE RACE")
         }else if dropDownEthicity.selectedIndex == 0{
            self.showAlert("PLEASE SELECT THE ETHICITY")
         }else if dropDownReferredBy.selectedIndex == 0{
            self.showAlert("PLEASE SELECT THE REFERRAL")
         } else{
            let step2ContactVC = patientStoryboard.instantiateViewControllerWithIdentifier("PatientRegistration5Vc") as! PatientRegistration5Vc
            patient.languageDropDown = dropDownLanguage.selectedIndex
            patient.raceDropDown = dropdownRace.selectedIndex
            patient.ethnicityDropDown = dropDownEthicity.selectedIndex
            patient.referralDropDown = dropDownReferredBy.selectedIndex
            patient.languageOther = self.otherLanguage
            patient.raceOther = self.otherRace
            patient.ethnicityOther = self.otherEthicity
            patient.referredOther = self.otherReferral
            
            patient.arrayLanguage = dropDownLanguage.selectedOption
            patient.arrayrace = dropdownRace.selectedOption
            patient.arrayethnicity = dropDownEthicity.selectedOption
            patient.arrayreferral = dropDownReferredBy.selectedOption
            step2ContactVC.patient = self.patient
            self.navigationController?.pushViewController(step2ContactVC, animated: true)
        }
    }
    
}
extension PatientRegistrationStep4Vc: BRDropDownDelegate {
    func dropDown(dropDown: BRDropDown, selectedAtIndex index: Int, selectedOption option: String?) {
        if dropDown == dropDownLanguage && index == 2 {
            PopupTextField.sharedInstance.showWithPlaceHolder("PLEASE SPECIFY", keyboardType: UIKeyboardType.Default, textFormat: TextFormat.Default, completion: { (textField, isEdited) in
                if !textField.isEmpty {
//                    self.dropDownLanguage.reset()
                    self.otherLanguage = textField.text!
                } else  {
                    self.otherLanguage = ""
                    self.dropDownLanguage.reset()
                }

            })
            
//            if !textField.isEmpty {
//                
//                //self.dropDownRelation.reset()
//                self.otherRelation = textField.text!
//                
//            } else {
//                
//                self.otherRelation = ""
//            }
//            
            
            
            
//            PopupTextField.popUpView().showWithTitle("PLEASE SPECIFY THE LANGUAGE", placeHolder: "LANGUAGE *", textFormat: TextFormat.Default, completion: { (textField, isEdited) in
//                if !textField.isEmpty {
//                    //self.dropDownLanguage.reset()
//                    self.otherLanguage = textField.text!
//                } else  {
//                    self.otherLanguage = ""
//                }x
//            })
            
        } else if dropDown == dropDownLanguage  {
            self.otherLanguage = option == nil ? "" : option!.uppercaseString
            
        }
        
        if dropDown == dropdownRace && index == 7 {
            PopupTextField.sharedInstance.showWithPlaceHolder("PLEASE SPECIFY", keyboardType: UIKeyboardType.Default, textFormat: TextFormat.Default, completion: { (textField, isEdited) in
                if !textField.isEmpty {
                    
                    self.otherRace = textField.text!
                } else  {
                    self.otherRace = ""
                    self.dropdownRace.reset()
                }
                
            })

//            PopupTextField.popUpView().showWithTitle("PLEASE SPECIFY THE RACE", placeHolder: "RACE *", textFormat: TextFormat.Default, completion: { (textField, isEdited) in
//                if !textField.isEmpty {
////                    self.dropdownRace.reset()
//                    self.otherRace = textField.text!
//                } else  {
//                    self.otherRace = ""
//                }
//            })
            
        } else if dropDown == dropdownRace {
            self.otherRace = option == nil ? "" : option!.uppercaseString
            
        }
        if dropDown == dropDownEthicity && index == 5 {
            PopupTextField.sharedInstance.showWithPlaceHolder("PLEASE SPECIFY", keyboardType: UIKeyboardType.Default, textFormat: TextFormat.Default, completion: { (textField, isEdited) in
                if !textField.isEmpty {
                   
                    self.otherEthicity = textField.text!
                } else {
                    self.otherEthicity = ""
                     self.dropDownEthicity.reset()
                }
                
            })

//            PopupTextField.popUpView().showWithTitle("PLEASE SPECIFY THE ETHNICITY", placeHolder: "ETHNICITY *", textFormat: TextFormat.Default, completion: { (textField, isEdited) in
//                if !textField.isEmpty {
////                    self.dropDownEthicity.reset()
//                    self.otherEthicity = textField.text!
//                } else {
//                    self.otherEthicity = ""
//                }
//            })
            
        } else if dropDown == dropDownEthicity {
            self.otherEthicity = option == nil ? "" : option!.uppercaseString
            
        }
        
        if dropDown == dropDownReferredBy && index == 15 {
            
            PopupTextField.sharedInstance.showWithPlaceHolder("PLEASE SPECIFY", keyboardType: UIKeyboardType.Default, textFormat: TextFormat.Default, completion: { (textField, isEdited) in
                if !textField.isEmpty {
                    
                    self.otherReferral = textField.text!
                } else {
                    self.otherReferral = ""
                    self.dropDownReferredBy.reset()
                }
                
            })

//            PopupTextField.popUpView().showWithTitle("PLEASE SPECIFY THE REFERREL", placeHolder: "REFERREDBY *", textFormat: TextFormat.Default, completion: { (textField, isEdited) in
//                if !textField.isEmpty {
////                    self.dropDownReferredBy.reset()
//                    self.otherReferral = textField.text!
//                } else {
//                    self.otherReferral = ""
//                }
//            })
            
        } else if dropDown == dropDownReferredBy {
            self.otherReferral = option == nil ? "" : option!.uppercaseString
        }
        
        
        
        
    }
    
}


