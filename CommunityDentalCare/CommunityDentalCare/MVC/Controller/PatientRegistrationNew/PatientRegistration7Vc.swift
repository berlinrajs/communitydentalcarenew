//
//  PatientRegistration7Vc.swift
//  CommunityDentalCare
//
//  Created by SRS Web Solutions on 28/09/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PatientRegistration7Vc: PDViewController {
    
    @IBOutlet var textFieldPolicyholderName: MCTextField!
    @IBOutlet var textFieldInsuredName: MCTextField!
    @IBOutlet var textFieldInsuredAddress: MCTextField!
    @IBOutlet var textFieldGroupId: MCTextField!
    @IBOutlet var textFieldPolicyId: MCTextField!
    @IBOutlet var textFieldPhoneNumber: MCTextField!
    @IBOutlet var textFieldDateOfBirth: MCTextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
         textFieldPhoneNumber.textFormat = .Phone
         textFieldDateOfBirth.textFormat = .DateIn1980
        
        //Autofill
        textFieldPolicyholderName.text = patient.primaryInsurance.policyholderName
        textFieldInsuredName.text = patient.primaryInsurance.insuredPlanName
        textFieldInsuredAddress.text = patient.primaryInsurance.address
        textFieldGroupId.text = patient.primaryInsurance.groupId
        textFieldPolicyId.text = patient.primaryInsurance.policyId
        textFieldPhoneNumber.text =  patient.primaryInsurance.phone
        textFieldDateOfBirth.text = patient.primaryInsurance.dateOfBirth 
        
        
        // Do any additional setup after loading the view.
    }

    func saveValues () {
        patient.primaryInsurance.policyholderName = textFieldPolicyholderName.text!
        patient.primaryInsurance.insuredPlanName = textFieldInsuredName.text!
        patient.primaryInsurance.address = textFieldInsuredAddress.text!
        patient.primaryInsurance.groupId = textFieldGroupId.text!
        patient.primaryInsurance.policyId = textFieldPolicyId.text!
        patient.primaryInsurance.phone = textFieldPhoneNumber.text!
        patient.primaryInsurance.dateOfBirth = textFieldDateOfBirth.text!
        
    }
    
    
    
    @IBAction override func buttonActionBack(withSender sender: UIButton) {
        saveValues()
        super.buttonActionBack(withSender: sender)
        
        
    }
    
    
    @IBAction func ButtonActionNext(withsender sender: AnyObject) {
        if !textFieldPhoneNumber.isEmpty && !textFieldPhoneNumber.text!.isPhoneNumber {
            self.showAlert("PLEASE ENTER A VALID PHONE NUMBER")
        } else if patient.radiobuttonTag2 == 3  {
        let step3VC = self.storyboard?.instantiateViewControllerWithIdentifier("PatientRegistration8Vc") as! PatientRegistration8Vc
            patient.primaryInsurance.policyholderName = textFieldPolicyholderName.text!
            patient.primaryInsurance.insuredPlanName = textFieldInsuredName.text!
            patient.primaryInsurance.address = textFieldInsuredAddress.text!
            patient.primaryInsurance.groupId = textFieldGroupId.text!
            patient.primaryInsurance.policyId = textFieldPolicyId.text!
            patient.primaryInsurance.phone = textFieldPhoneNumber.text!
            patient.primaryInsurance.dateOfBirth = textFieldDateOfBirth.text!
        step3VC.patient = self.patient
        self.navigationController?.pushViewController(step3VC, animated: true)
        } else {
            let step3VC = self.storyboard?.instantiateViewControllerWithIdentifier("PatientRegistration9Vc") as! PatientRegistration9Vc
            patient.primaryInsurance.policyholderName = textFieldPolicyholderName.text!
            patient.primaryInsurance.insuredPlanName = textFieldInsuredName.text!
            patient.primaryInsurance.address = textFieldInsuredAddress.text!
            patient.primaryInsurance.groupId = textFieldGroupId.text!
            patient.primaryInsurance.policyId = textFieldPolicyId.text!
            patient.primaryInsurance.phone = textFieldPhoneNumber.text!
            patient.primaryInsurance.dateOfBirth = textFieldDateOfBirth.text!
            step3VC.patient = self.patient
            self.navigationController?.pushViewController(step3VC, animated: true)
            
            
        }
    
    
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
