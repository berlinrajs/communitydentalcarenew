
//
//  PatientRegistrationStep1Vc.swift
//  CommunityDentalCare
//
//  Created by SRS Web Solutions on 27/09/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PatientRegistrationStep1Vc: PDViewController {

    @IBOutlet weak var textFieldFirstName: MCTextField!
    @IBOutlet weak var textFieldLastName: MCTextField!
    @IBOutlet var textFieldPhoneNumber: MCTextField!
    @IBOutlet var textFieldAddress: MCTextField!
    @IBOutlet var dropdownRelation: BRDropDown!
    @IBOutlet weak var textFieldDate: MCTextField!
    @IBOutlet weak var textFieldYear: MCTextField!
    @IBOutlet weak var textFieldMonth: MCTextField!
    @IBOutlet weak var labelDateOfBirth: UILabel!
    @IBOutlet weak var radioParent: RadioButton!
    @IBOutlet var labelpatientOrGuardian: UILabel!
    
    @IBOutlet var radioButtonNo: RadioButton!
    
    var isSecondGuardian : Bool = false
    var guardian2 : GuardianPage2!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dropdownRelation.items = ["FATHER", "MOTHER", "LEGAL GUARDIAN"]
     
        dropdownRelation.placeholder = "-- RELATION *--"
        
        textFieldDate.textFormat = .Date
        textFieldYear.textFormat = .Year
        textFieldMonth.textFormat = .Month
        textFieldPhoneNumber.textFormat = .Phone
        if  patient.parentRadioButton != nil{
            radioParent.setSelectedWithTag(patient.parentRadioButton)
        }
        
        if radioParent.selectedButton.tag == 1 {
            textFieldFirstName.enabled = true
            textFieldLastName.enabled = true
            textFieldDate.enabled = true
            textFieldMonth.enabled = true
            textFieldYear.enabled = true
            textFieldPhoneNumber.enabled = true
            textFieldAddress.enabled = true
            dropdownRelation.userInteractionEnabled = true
            dropdownRelation.alpha = 1
            labelDateOfBirth.enabled = true
        } else {
            textFieldFirstName.text = ""
            textFieldLastName.text = ""
            textFieldDate.text = ""
            textFieldMonth.text = ""
            textFieldYear.text = ""
            textFieldPhoneNumber.text = ""
            textFieldAddress.text = ""
            
            textFieldFirstName.enabled = false
            textFieldLastName.enabled = false
            textFieldDate.enabled = false
            textFieldMonth.enabled = false
            textFieldYear.enabled = false
            labelDateOfBirth.enabled = false
            textFieldPhoneNumber.enabled = false
            textFieldAddress.enabled = false
            dropdownRelation.placeholder = "-- RELATION *--"
            dropdownRelation.userInteractionEnabled = false
            dropdownRelation.alpha = 0.5
        }
        //AutoFill
        
        textFieldFirstName.text = patient.parentFirstName
        textFieldLastName.text = patient.parentLastName
        textFieldPhoneNumber.text = patient.parentPhone
        textFieldAddress.text = patient.parentAddress
        if patient.parentDateOfBirth != nil {
            splitDateOfBirth(
                patient.parentDateOfBirth!)
        }
        
        if radioParent.selected &&  patient.parentRelationType  != "" {
            
          dropdownRelation.setSelectedOption = patient.parentRelationType
        }
        
        
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func saveValues () {
        self.patient.parentFirstName = self.radioParent.selected ? self.textFieldFirstName.text! : ""
        self.patient.parentLastName = self.radioParent.selected ? self.textFieldLastName.text! : ""
        self.patient.parentDateOfBirth = self.radioParent.selected ? self.getDateOfBirth() : ""
        self.patient.parentAddress = self.radioParent.selected ? self.textFieldAddress.text! : ""
        self.patient.parentPhone = self.radioParent.selected ? self.textFieldPhoneNumber.text! : ""
//        self.patient.parentRelationType = self.dropdownRelation.selectedOption
        self.patient.parentRadioButton = self.radioParent.selectedButton.tag
        self.patient.parentRelationType2 = self.dropdownRelation.selectedIndex
        
        if radioParent.selected &&  dropdownRelation.selectedOption  != nil {
            
           patient.parentRelationType =  dropdownRelation.selectedOption!
            
            
            
            
        }
    }
    
    
    
    @IBAction override func buttonActionBack(withSender sender: UIButton) {
        saveValues()
       super.buttonActionBack(withSender: sender)
    }
    
    
    
    
    
    @IBAction func radioParentAction() {
        if radioParent.selected {
            textFieldFirstName.enabled = true
            textFieldLastName.enabled = true
            textFieldDate.enabled = true
            textFieldMonth.enabled = true
            textFieldYear.enabled = true
            textFieldPhoneNumber.enabled = true
            textFieldAddress.enabled = true
            dropdownRelation.userInteractionEnabled = true
            dropdownRelation.alpha = 1
            labelDateOfBirth.enabled = true
        } else {
            textFieldFirstName.text = ""
            textFieldLastName.text = ""
            textFieldDate.text = ""
            textFieldMonth.text = ""
            textFieldYear.text = ""
            textFieldPhoneNumber.text = ""
            textFieldAddress.text = ""
            
            
            
            textFieldFirstName.enabled = false
            textFieldLastName.enabled = false
            textFieldDate.enabled = false
            textFieldMonth.enabled = false
            textFieldYear.enabled = false
            labelDateOfBirth.enabled = false
            textFieldPhoneNumber.enabled = false
            textFieldAddress.enabled = false
            dropdownRelation.placeholder = "-- RELATION *--"
            dropdownRelation.userInteractionEnabled = false
            dropdownRelation.alpha = 0.5
        }
    }
    
    @IBAction func buttonNextAction() {
        self.view.endEditing(true)
        if radioParent.selected && (textFieldFirstName.isEmpty || textFieldLastName.isEmpty) {
            self.showAlert("PLEASE ENTER YOUR NAME")
        } else if !textFieldPhoneNumber.isEmpty && !textFieldPhoneNumber.text!.isPhoneNumber {
                self.showAlert("PLEASE ENTER A VALID PHONE NUMBER")
        } else if radioParent.selected && dropdownRelation.selectedIndex == 0 {
            self.showAlert("PLEASE SELECT RELATION")
        } else if radioParent.selected && invalidDateofBirth {
            self.showAlert("PLEASE ENTER THE VALID DATE OF BIRTH")
        } else {
            if radioParent.selected {
                YesOrNoAlert.sharedInstance.showWithTitle("DO YOU HAVE ANOTHER GUARDIAN", button1Title: "YES", button2Title: "NO", completion: { (buttonIndex) in
                    if buttonIndex == 1{
                        let newPatientStep5VC = self.storyboard?.instantiateViewControllerWithIdentifier("PatientRegistrationGuardian2Vc") as! PatientRegistrationGuardian2Vc
                        self.patient.parentFirstName = self.radioParent.selected ? self.textFieldFirstName.text! : ""
                        self.patient.parentLastName = self.radioParent.selected ? self.textFieldLastName.text! : ""
                        self.patient.parentDateOfBirth = self.radioParent.selected ? self.getDateOfBirth() : ""
                        self.patient.parentAddress = self.radioParent.selected ? self.textFieldAddress.text! : ""
                        self.patient.parentPhone = self.radioParent.selected ? self.textFieldPhoneNumber.text! : ""
                       self.patient.parentRelationType = self.dropdownRelation.selectedOption!
                        self.patient.parentRadioButton = self.radioParent.selectedButton.tag
                        self.patient.parentRelationType2 = self.dropdownRelation.selectedIndex
                        newPatientStep5VC.patient = self.patient
               self.navigationController?.pushViewController(newPatientStep5VC, animated: true)
                    }else{
                        self.patient.parentFirstName = self.radioParent.selected ? self.textFieldFirstName.text! : ""
                        self.patient.parentLastName = self.radioParent.selected ? self.textFieldLastName.text! : ""
                        self.patient.parentDateOfBirth = self.radioParent.selected ? self.getDateOfBirth() : ""
                        self.patient.parentAddress = self.radioParent.selected ? self.textFieldAddress.text! : ""
                        self.patient.parentPhone = self.radioParent.selected ? self.textFieldPhoneNumber.text! : ""
                        self.patient.parentRelationType = self.dropdownRelation.selectedOption!
                        self.patient.parentRadioButton = self.radioParent.selectedButton.tag
                        self.patient.parentRelationType2 = self.dropdownRelation.selectedIndex
                        let newPatientStep5VC = self.storyboard?.instantiateViewControllerWithIdentifier("PatientRegistrationStep2Vc") as! PatientRegistrationStep2Vc
                        newPatientStep5VC.patient = self.patient
                        self.navigationController?.pushViewController(newPatientStep5VC, animated: true)
                    }
                })
            }else{
                let newPatientStep5VC = self.storyboard?.instantiateViewControllerWithIdentifier("PatientRegistrationStep2Vc") as! PatientRegistrationStep2Vc
                self.patient.parentFirstName = self.radioParent.selected ? self.textFieldFirstName.text! : ""
                self.patient.parentLastName = self.radioParent.selected ? self.textFieldLastName.text! : ""
                self.patient.parentDateOfBirth = self.radioParent.selected ? self.getDateOfBirth() : ""
                self.patient.parentAddress = self.radioParent.selected ? self.textFieldAddress.text! : ""
                self.patient.parentPhone = self.radioParent.selected ? self.textFieldPhoneNumber.text! : ""
                if radioParent.selected &&  dropdownRelation.selectedOption  != nil {
                    
                    patient.parentRelationType =  dropdownRelation.selectedOption!
                }
//                self.patient.parentRelationType = self.dropdownRelation.selectedOption!
                self.patient.parentRadioButton = self.radioParent.selectedButton.tag
                self.patient.parentRelationType2 = self.dropdownRelation.selectedIndex
                newPatientStep5VC.patient = self.patient
                self.navigationController?.pushViewController(newPatientStep5VC, animated: true)
                
            }
        }
    }
    
    func  splitDateOfBirth(datebirth : String)  {
        let arrayString = datebirth.componentsSeparatedByString(" ")
        if arrayString.count == 3{
            textFieldMonth.text = arrayString[0]
            textFieldDate.text = arrayString[1].stringByReplacingOccurrencesOfString(",", withString: "")
            textFieldYear.text = arrayString[2]
        }
        
    }
    func getDateOfBirth() -> String {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy"
        if !textFieldDate.isEmpty && !textFieldMonth.isEmpty && !textFieldYear.isEmpty{
        let dob = dateFormatter.dateFromString(textFieldMonth.text! + " " + textFieldDate.text! + ", " + textFieldYear.text!)!
        let dateFormatter1 = NSDateFormatter()
        dateFormatter1.dateFormat = kCommonDateFormat
        return dateFormatter1.stringFromDate(dob)
    }
     return ""
    }
    
    var invalidDateofBirth: Bool {
        get {
            if textFieldMonth.isEmpty || textFieldDate.isEmpty || textFieldYear.isEmpty {
                return true
            } else if Int(textFieldDate.text!)! == 0 {
                return true
            } else if !textFieldYear.text!.isValidYear {
                return true
            } else {
                
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "dd-MMM-yyyy"
                
                let todayDate = dateFormatter.dateFromString(dateFormatter.stringFromDate(NSDate()))
                let currentDate = dateFormatter.dateFromString("\(textFieldDate.text!)-\(textFieldMonth.text!)-\(textFieldYear.text!)")
                
                if todayDate == nil || currentDate == nil {
                    return true
                }
                if todayDate!.timeIntervalSinceDate(currentDate!) < 0 {
                    return true
                }
                return false
            }
        }
    }
}

