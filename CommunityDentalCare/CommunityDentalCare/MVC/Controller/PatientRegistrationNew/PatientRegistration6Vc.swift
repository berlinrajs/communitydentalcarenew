//
//  PatientRegistration6Vc.swift
//  CommunityDentalCare
//
//  Created by SRS Web Solutions on 28/09/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PatientRegistration6Vc: PDViewController {
    
    
    @IBOutlet weak var radioPrimary: RadioButton!
    @IBOutlet weak var radioSecondary: RadioButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if  patient.radiobuttonTag != nil{
        radioPrimary.setSelectedWithTag(patient.radiobuttonTag)
        }
        
        if  patient.radiobuttonTag2 != nil{
            radioSecondary.setSelectedWithTag(patient.radiobuttonTag2)
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func saveValues () {
        
    }
    
    
    
    @IBAction override func buttonActionBack(withSender sender: UIButton) {
        saveValues()
        super.buttonActionBack(withSender: sender)
        
        
    }

    
    
    @IBAction func RadioButtonAction(withsender sender: RadioButton) {
        patient.radiobuttonTag = sender.selectedButton.tag
        
    }
    
    @IBAction func RadioButtonAction2(withsender sender: RadioButton) {
        patient.radiobuttonTag2 = sender.selectedButton.tag
        
    }
    
    
    @IBAction func buttonNextAction() {
        if radioPrimary.selectedButton == nil {
            self.showAlert("PLEASE SELECT ALL THE REQUIRED")
        } else if radioSecondary.selectedButton == nil {
            self.showAlert("PLEASE SELECT ALL THE REQUIRED")
        } else {
            if patient.radiobuttonTag == 1 {
                let step3VC = self.storyboard?.instantiateViewControllerWithIdentifier("PatientRegistration7Vc") as! PatientRegistration7Vc
                
                step3VC.patient = self.patient
                self.navigationController?.pushViewController(step3VC, animated: true)
            } else if patient.radiobuttonTag2 == 3 {
                let step3VC = self.storyboard?.instantiateViewControllerWithIdentifier("PatientRegistration8Vc") as! PatientRegistration8Vc
                step3VC.patient = self.patient
                self.navigationController?.pushViewController(step3VC, animated: true)
            } else {
                let step3VC = self.storyboard?.instantiateViewControllerWithIdentifier("PatientRegistration9Vc") as! PatientRegistration9Vc
                step3VC.patient = self.patient
                self.navigationController?.pushViewController(step3VC, animated: true)
                
            }
        }
    }
}
