//
//  PatientRegistrationGuardian2Vc.swift
//  CommunityDentalCare
//
//  Created by SRS Web Solutions on 28/09/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PatientRegistrationGuardian2Vc: PDViewController {
    @IBOutlet weak var textFieldFirstName: MCTextField!
    @IBOutlet weak var textFieldLastName: MCTextField!
    @IBOutlet var textFieldPhoneNumber: MCTextField!
    @IBOutlet var textFieldAddress: MCTextField!
    @IBOutlet var dropdownRelation: BRDropDown!
    @IBOutlet weak var textFieldDate: MCTextField!
    @IBOutlet weak var textFieldYear: MCTextField!
    @IBOutlet weak var textFieldMonth: MCTextField!
    @IBOutlet weak var labelDateOfBirth: UILabel!
    
    var guardian2 : GuardianPage2!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dropdownRelation.items = ["FATHER", "MOTHER", "LEGAL GUARDIAN"]
        dropdownRelation.placeholder = "-- RELATION *--"
        textFieldDate.textFormat = .Date
        textFieldYear.textFormat = .Year
        textFieldMonth.textFormat = .Month
        textFieldPhoneNumber.textFormat = .Phone
        
        //Autofill
        
        textFieldFirstName.text = patient.guardian2.firstName
        textFieldLastName.text = patient.guardian2.lastName
        textFieldPhoneNumber.text = patient.guardian2.phoneNumber
        textFieldAddress.text = patient.guardian2.address
        if patient.guardian2.dateOfBirth != nil {
            splitDateOfBirth(
                patient.guardian2.dateOfBirth!)
        }
        
//        if  patient.guardian2.relationtype2 != nil {
//            
//            dropdownRelation.setSelectedOption = patient.guardian2.relation
//        }
        
        if patient.guardian2.relation != nil && dropdownRelation.items.contains(patient.guardian2.relation){
            dropdownRelation.setSelectedOption = patient.guardian2.relation
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func saveValues () {
        patient.guardian2.firstName = textFieldFirstName.text!
        patient.guardian2.lastName = textFieldLastName.text!
        patient.guardian2.phoneNumber = textFieldPhoneNumber.text!
        patient.guardian2.address = textFieldAddress.text!
//        patient.guardian2.relation = dropdownRelation.selectedOption!
        
        patient.guardian2.relationtype2 = dropdownRelation.selectedIndex
        
        
        if patient.guardian2.relationtype2 != nil{
            patient.guardian2.relation =  dropdownRelation.selectedOption == nil ? "-- RELATION *--" : dropdownRelation.selectedOption!
        }
        
        if patient.guardian2.dateOfBirth != nil{
          patient.guardian2.dateOfBirth = getDateOfBirth()
        }
        self.patient.guardian2.dateOfBirth = getDateOfBirth()
        
    }
    
    
    
    @IBAction override func buttonActionBack(withSender sender: UIButton) {
        saveValues()
        super.buttonActionBack(withSender: sender)
        
    }

    
    
    
    
    @IBAction func buttonNextAction() {
        self.view.endEditing(true)
        if textFieldFirstName.isEmpty || textFieldLastName.isEmpty {
            self.showAlert("PLEASE ENTER YOUR NAME")
        } else if !textFieldPhoneNumber.isEmpty && !textFieldPhoneNumber.text!.isPhoneNumber {
        self.showAlert("PLEASE ENTER A VALID PHONE NUMBER")
        } else if dropdownRelation.selectedIndex == 0 {
            self.showAlert("PLEASE SELECT RELATION")
        } else if invalidDateofBirth {
            self.showAlert("PLEASE ENTER THE VALID DATE OF BIRTH")
        } else {
            patient.guardian2.firstName = textFieldFirstName.text!
            patient.guardian2.lastName = textFieldLastName.text!
            patient.guardian2.phoneNumber = textFieldPhoneNumber.text!
            patient.guardian2.address = textFieldAddress.text!
            patient.guardian2.relation = dropdownRelation.selectedOption!
            patient.guardian2.relationtype2 = dropdownRelation.selectedIndex
            patient.guardian2.dateOfBirth = getDateOfBirth()
            let newPatientStep5VC = self.storyboard?.instantiateViewControllerWithIdentifier("PatientRegistrationStep2Vc") as! PatientRegistrationStep2Vc
            newPatientStep5VC.patient = self.patient
            self.navigationController?.pushViewController(newPatientStep5VC, animated: true)
            
        }
    }
    func  splitDateOfBirth(datebirth : String)  {
        let arrayString = datebirth.componentsSeparatedByString(" ")
        textFieldMonth.text = arrayString[0]
        textFieldDate.text = arrayString[1].stringByReplacingOccurrencesOfString(",", withString: "")
        textFieldYear.text = arrayString[2]
        
    }
    
    func getDateOfBirth() -> String {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy"
        if !textFieldDate.isEmpty && !textFieldMonth.isEmpty && !textFieldYear.isEmpty{
        let dob = dateFormatter.dateFromString(textFieldMonth.text! + " " + textFieldDate.text! + ", " + textFieldYear.text!)!
        let dateFormatter1 = NSDateFormatter()
        dateFormatter1.dateFormat = kCommonDateFormat
        return dateFormatter1.stringFromDate(dob)
    }
        return ""
    }
    var invalidDateofBirth: Bool {
        get {
            if textFieldMonth.isEmpty || textFieldDate.isEmpty || textFieldYear.isEmpty {
                return true
            } else if Int(textFieldDate.text!)! == 0 {
                return true
            } else if !textFieldYear.text!.isValidYear {
                return true
            } else {
                
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "dd-MMM-yyyy"
                
                let todayDate = dateFormatter.dateFromString(dateFormatter.stringFromDate(NSDate()))
                let currentDate = dateFormatter.dateFromString("\(textFieldDate.text!)-\(textFieldMonth.text!)-\(textFieldYear.text!)")
                
                if todayDate == nil || currentDate == nil {
                    return true
                }
                if todayDate!.timeIntervalSinceDate(currentDate!) < 0 {
                    return true
                }
                return false
            }
        }
    }
}
