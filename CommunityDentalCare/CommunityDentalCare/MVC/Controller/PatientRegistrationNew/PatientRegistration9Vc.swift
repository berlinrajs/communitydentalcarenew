//
//  PatientRegistration9Vc.swift
//  CommunityDentalCare
//
//  Created by SRS Web Solutions on 28/09/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PatientRegistration9Vc: PDViewController {
    
    @IBOutlet var textFieldWitnessName: MCTextField!
    @IBOutlet var textFieldPhoneNumber: MCTextField!
    @IBOutlet var imageViewSignaturee: SignatureView!
    @IBOutlet var labelDate: DateLabel!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate.todayDate = patient.dateToday
        textFieldPhoneNumber.textFormat = .Phone
        
        //Autofill
        textFieldWitnessName.text = patient.witnessName
        textFieldPhoneNumber.text = patient.witnessPhone
        
    }
    
    func saveValues () {
        patient.signature = imageViewSignaturee.image
        patient.witnessName = textFieldWitnessName.text
        patient.witnessPhone = textFieldPhoneNumber.text
    }
    
    
    
    @IBAction override func buttonActionBack(withSender sender: UIButton) {
        saveValues()
        super.buttonActionBack(withSender: sender)
    }

    
    @IBAction func ButtonActionNext(withsender sender: AnyObject) {
        if !textFieldPhoneNumber.isEmpty && !textFieldPhoneNumber.text!.isPhoneNumber {
            self.showAlert("PLEASE ENTER A VALID PHONE NUMBER")
        }
        else if !imageViewSignaturee.isSigned(){
            self.showAlert("PLEASE SIGN THE FORM")
        }
        else if !labelDate.dateTapped {
            self.showAlert("PLEASE SELECT THE DATE")
    } else {
//    let step3VC = self.storyboard?.instantiateViewControllerWithIdentifier("PatientRegistrationForm") as! PatientRegistrationForm
//    step3VC.patient = self.patient
//    self.navigationController?.pushViewController(step3VC, animated: true)
            saveValues()
            let privacy = mainStoryboard.instantiateViewControllerWithIdentifier("Privacy2VC") as! PrivacyPractice2ViewController
            privacy.patient = self.patient
            self.navigationController?.pushViewController(privacy, animated: true)

    
    }
}




    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
}
