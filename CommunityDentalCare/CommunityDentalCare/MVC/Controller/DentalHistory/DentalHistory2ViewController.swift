//
//  DentalHistory2ViewController.swift
//  CommunityDentalCare
//
//  Created by Bala Murugan on 9/28/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class DentalHistory2ViewController: PDViewController {

    @IBOutlet var tableViewQuestions: UITableView!
    @IBOutlet var buttonVerified: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonVerifiedAction(withSender sender: UIButton) {
        sender.selected = !sender.selected
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if buttonVerified.selected == false {
            let alert = Extention.alert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            let dental = medicalStoryboard.instantiateViewControllerWithIdentifier("Dental3VC") as! DentalHistory3ViewController
            dental.patient = self.patient
            self.navigationController?.pushViewController(dental, animated: true)
            }
        }


}

extension DentalHistory2ViewController : UITableViewDataSource,UITableViewDelegate {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patient.dentalHistory.dentalQuestions.count
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 45
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! PatientInfoCell
        cell.delegate = self
        cell.tag = indexPath.row
        cell.configCell(self.patient.dentalHistory.dentalQuestions[indexPath.row])
        
        return cell
    }
}

extension DentalHistory2ViewController : PatientInfoCellDelegate{
    
    func radioButtonTappedForCell(cell: PatientInfoCell) {
        PopupTextField.sharedInstance.showWithPlaceHolder("WHICH SIDE?", keyboardType: UIKeyboardType.Default, textFormat: TextFormat.Default) { (textField, isEdited) in
            if isEdited{
                self.patient.dentalHistory.dentalQuestions[cell.tag].answer = textField.text
            }else{
                cell.radioButtonYes.selected = false
            }

        }
    }
}

