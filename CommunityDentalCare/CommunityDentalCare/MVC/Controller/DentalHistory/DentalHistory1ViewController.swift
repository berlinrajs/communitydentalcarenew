//
//  DentalHistory1ViewController.swift
//  CommunityDentalCare
//
//  Created by Bala Murugan on 9/28/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class DentalHistory1ViewController: PDViewController {
    @IBOutlet weak var textviewReason : UITextView!
    @IBOutlet weak var radioToothBrush : RadioButton!
    @IBOutlet weak var textfieldDentalCheckup : UITextField!
    @IBOutlet weak var textfieldXray : UITextField!
    @IBOutlet weak var textfieldExam : UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        DateInputView.addDatePickerForTextField(textfieldDentalCheckup)
        DateInputView.addDatePickerForTextField(textfieldXray)
        DateInputView.addDatePickerForTextField(textfieldExam)

        loadValues()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func saveValues (){
        patient.dentalHistory.dentalReason = textviewReason.text!
        patient.dentalHistory.toothBrushTag = radioToothBrush.selectedButton == nil ? 0 : radioToothBrush.selectedButton.tag
        patient.dentalHistory.dentalCheckup = textfieldDentalCheckup.isEmpty ? "N/A" : textfieldDentalCheckup.text!
        patient.dentalHistory.dentalXray = textfieldXray.isEmpty ? "N/A" : textfieldXray.text!
        patient.dentalHistory.dentalCleaning = textfieldExam.isEmpty ? "N/A" : textfieldExam.text!

    }
    
    func loadValues()  {
        textviewReason.text = patient.dentalHistory.dentalReason == "" ? "PLEASE TYPE HERE" : patient.dentalHistory.dentalReason
        if textviewReason.text == "PLEASE TYPE HERE"{
            textviewReason.textColor = UIColor.lightGrayColor()
        }else{
            textviewReason.textColor = UIColor.blackColor()
        }
        radioToothBrush.setSelectedWithTag(patient.dentalHistory.toothBrushTag)
        textfieldDentalCheckup.setSavedText(patient.dentalHistory.dentalCheckup)
        textfieldXray.setSavedText(patient.dentalHistory.dentalXray)
        textfieldExam.setSavedText(patient.dentalHistory.dentalCleaning)
        
    }
    
    @IBAction override func buttonActionBack(withSender sender : UIButton) {
        saveValues()
        super.buttonActionBack(withSender: sender)
        ///self.navigationController?.popViewControllerAnimated(true)
    }


    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if textviewReason.text == "PLEASE TYPE HERE" {
            let alert = Extention.alert("PLEASE ENTER ALL THE REQUIRED FIELDS")
            self.presentViewController(alert, animated: true, completion: nil)
        }else{
        saveValues()
        let dental = medicalStoryboard.instantiateViewControllerWithIdentifier("Dental2VC") as! DentalHistory2ViewController
        dental.patient = self.patient
        self.navigationController?.pushViewController(dental, animated: true)
        }
    }

}

extension DentalHistory1ViewController : UITextViewDelegate {
    
    func textViewDidBeginEditing(textView: UITextView) {
        if textView.text == "PLEASE TYPE HERE" {
            textView.text = ""
            textView.textColor = UIColor.blackColor()
        }
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "PLEASE TYPE HERE"
            textView.textColor = UIColor.lightGrayColor()
        }
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}

extension DentalHistory1ViewController: UITextFieldDelegate {
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
}

