//
//  PDQuestion.swift
//  Advanced Aesthetic Dentistry
//
//  Created by Leojin Bose on 2/26/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PDQuestion: NSObject {
    var question : String!
    var isAnswerRequired : Bool!
    var answer : String?
    var selectedOption : Bool? = false {
        didSet {
            if selectedOption == false {
                answer = nil
            }
        }
    }

    init(question: String) {
        super.init()
        self.question = question
        self.isAnswerRequired = false
        self.selectedOption = false
    }

    init(dict : NSDictionary) {
        super.init()
        self.question = dict["question"] as! String
        self.isAnswerRequired = (dict["verification"] as! String) == "Yes"
    }
    
    class func getObjects (arrayResult : NSArray) -> [PDQuestion] {
        var questions  = [PDQuestion]()
        for dict in arrayResult {
            let obj = PDQuestion(dict: dict as! NSDictionary)
            obj.selectedOption = false
            questions.append(obj)
        }
        return questions
    }
    
    class func arrayOfQuestions(array: [String]) -> [PDQuestion]{
        var questions = [PDQuestion]()
        for string in array {
            let question = PDQuestion(question: string)
            question.selectedOption = false
            questions.append(question)
        }
        return questions
    }
    

    class func getJSONObject(responseString : String) -> AnyObject? {
        do {
            let object = try NSJSONSerialization.JSONObjectWithData(responseString.dataUsingEncoding(NSUTF8StringEncoding)!, options: NSJSONReadingOptions.AllowFragments)
            return object
        } catch {
            return nil
        }
    }

    
    class func fetchQuestionsForm1(completion:(result : [PDQuestion]?, success : Bool) -> Void) {
        
        let responseString = "{\"Status\":\"success\",\"posts\":[{\"question\":\"Are you currently under the care of a physician?\",\"verification\":\"Yes\"},{\"question\":\"Do you use tobacco in any form?\",\"verification\":\"Yes\"},{\"question\":\"Have you had any metal rods,pins or implants placed?\",\"verification\":\"Yes\"},{\"question\":\"Are you taking any medications?\",\"verification\":\"Yes\"},{\"question\":\"Do you take daily aspirin: \",\"verification\":\"Yes\"},{\"question\":\"Have you ever had any surgical procedures?\",\"verification\":\"Yes\"}]}"
        
        
        let jsonObject = PDQuestion.getJSONObject(responseString)
        completion(result: self.getObjects(jsonObject!["posts"] as! NSArray), success: true)
        

    }
    
    class func fetchQuestionsForm2(completion:(result : [PDQuestion]?, success : Bool) -> Void) {
        
        let responseString = "{\"Status\":\"success\",\"posts\":[{\"question\":\"Rheumatic fever\",\"verification\":\"Yes\"},{\"question\":\"Rheumatic heart disease?\",\"verification\":\"Yes\"},{\"question\":\"Anemia, leukemia or low platelets\",\"verification\":\"Yes\"},{\"question\":\"Epilepsy or convulsions\",\"verification\":\"Yes\"},{\"question\":\"Asthma or hay fever?\",\"verification\":\"Yes\"},{\"question\":\"Tuberculosis?\",\"verification\":\"Yes\"},{\"question\":\"Diabetes? How long\",\"verification\":\"No\"},{\"question\":\"Kidney trouble?\",\"verification\":\"No\"},{\"question\":\"Liver trouble or jaundice?\",\"verification\":\"No\"},{\"question\":\"Thyroid trouble or goiter?\",\"verification\":\"No\"},{\"question\":\"Syphilis?\",\"verification\":\"No\"},{\"question\":\"Fainting or dizziness?\",\"verification\":\"No\"},{\"question\":\"Glaucoma?\",\"verification\":\"No\"}]}"
        
        
        let jsonObject = PDQuestion.getJSONObject(responseString)
        completion(result: self.getObjects(jsonObject!["posts"] as! NSArray), success: true)
        
        
    }
    
    class func fetchQuestionsForm3(completion:(result : [PDQuestion]?, success : Bool) -> Void) {
        
        let responseString = "{\"Status\":\"success\",\"posts\":[{\"question\":\"Arthritis?\",\"verification\":\"No\"},{\"question\":\"HIV AIDS?\",\"verification\":\"No\"},{\"question\":\"Stroke? \",\"verification\":\"Yes\"},{\"question\":\"Stomach ulcer?\",\"verification\":\"Yes\"},{\"question\":\"Heart murmur?\",\"verification\":\"Yes\"},{\"question\":\"Prostate trouble?\",\"verification\":\"Yes\"},{\"question\":\"Hepatitis A, B, C or D\",\"verification\":\"Yes\"},{\"question\":\"Eczema or hives?\",\"verification\":\"Yes\"},{\"question\":\" Psychiatric treatment?\",\"verification\":\"No\"},{\"question\":\"Blood Transfusion\",\"verification\":\"No\"},{\"question\":\"Alcohol abuse\",\"verification\":\"No\"},{\"question\":\"Recreational Drug use\",\"verification\":\"No\"},{\"question\":\"Are you pregnant?\",\"verification\":\"No\"}]}"
        
        
        let jsonObject = PDQuestion.getJSONObject(responseString)
        completion(result: self.getObjects(jsonObject!["posts"] as! NSArray), success: true)
        
        
    }

    
    class func fetchQuestionsForm4(completion:(result : [PDQuestion]?, success : Bool) -> Void) {
        
        let responseString = "{\"Status\":\"success\",\"posts\":[{\"question\":\"Emphysema\",\"verification\":\"Yes\"},{\"question\":\"Frequent Headaches\",\"verification\":\"Yes\"},{\"question\":\"STD's\",\"verification\":\"Yes\"},{\"question\":\"Chemo or Radiation Therapy\",\"verification\":\"Yes\"},{\"question\":\"Joint Replacement\",\"verification\":\"Yes\"},{\"question\":\"Fever Blisters or Shingles\",\"verification\":\"Yes\"},{\"question\":\"Colitis\",\"verification\":\"No\"},{\"question\":\"Facial Surgery\",\"verification\":\"No\"},{\"question\":\"Difficulty Breathing\",\"verification\":\"No\"},{\"question\":\"Sickle Cell Disease\",\"verification\":\"No\"},{\"question\":\" Seizures\",\"verification\":\"No\"}]}"
        
        
        let jsonObject = PDQuestion.getJSONObject(responseString)
        completion(result: self.getObjects(jsonObject!["posts"] as! NSArray), success: true)
        
        
    }

    
    class func fetchQuestionsForm5(completion:(result : [PDQuestion]?, success : Bool) -> Void) {
        
        let responseString = "{\"Status\":\"success\",\"posts\":[{\"question\":\"Drugs for high blood pressure?\",\"verification\":\"Yes\"},{\"question\":\"Drugs for sleep?\",\"verification\":\"Yes\"},{\"question\":\"Cortisone, steroids or ACTH?\",\"verification\":\"Yes\"},{\"question\":\"Anticoagulants or blood thinner?\",\"verification\":\"Yes\"},{\"question\":\"Tranquilizers or sedatives?\",\"verification\":\"Yes\"},{\"question\":\" Antibiotics?\",\"verification\":\"Yes\"},{\"question\":\" Insulin?\",\"verification\":\"Yes\"},{\"question\":\"Have you ever taken Fen-Phen?\",\"verification\":\"No\"},{\"question\":\"Others?\",\"verification\":\"No\"},{\"question\":\"Do you have any questions?\",\"verification\":\"No\"},{\"question\":\" Have you been under the care of a physician for any major illness or injury other than those noted above. If so, list\",\"verification\":\"No\"}]}"
        
        
        let jsonObject = PDQuestion.getJSONObject(responseString)
        completion(result: self.getObjects(jsonObject!["posts"] as! NSArray), success: true)
        
        
    }

}




class PDOption : NSObject {
    var question : String!
    var isSelected : Bool?
    var index : Int!
    
    init(value : String) {
        super.init()
        self.question = value
    }
    
    class func getObjects (arrayResult : NSArray) -> [PDOption] {
        var questions  = [PDOption]()
        for (idx, value) in arrayResult.enumerate() {
            let obj = PDOption(value: value as! String)
            obj.isSelected = false
            obj.index = idx
            questions.append(obj)
        }
        return questions
    }
    
    
    class func fetchQuestionsForm2(completion:(result : [PDOption]?, success : Bool) -> Void) {
        let responseString = "{\"Status\":\"success\", \"posts\":[\"Pregnant \\/ trying to get pregnant\", \"Nursing\", \"Taking oral contraceptives\", \"None\"]}"
        
        let jsonObject = PDQuestion.getJSONObject(responseString)
        completion(result: self.getObjects(jsonObject!["posts"] as! NSArray), success: true)
        
//        ServiceManager.fetchDataFromService("women_api.php", parameters: nil, success: { (result) -> Void in
//            completion(result: self.getObjects(result["posts"] as! NSArray), success: true)
//            print(result)
//            }) { (error) -> Void in
//                completion(result: nil, success: false)
//        }
    }
    
    class func fetchQuestionsForm3(completion:(result : [PDOption]?, success : Bool) -> Void) {
        
        let responseString = "{\"Status\":\"success\",\"posts\":[\"Aspirin\",\"Penicillin\",\"Codeine\",\"Acrylic\",\"Metal\",\"Latex\",\"Sulfa drugs\",\"Local anesthetics\", \"Others\"]}"
        
        let jsonObject = PDQuestion.getJSONObject(responseString)
        completion(result: self.getObjects(jsonObject!["posts"] as! NSArray), success: true)
        
        
//        ServiceManager.fetchDataFromService("alergic_api.php", parameters: nil, success: { (result) -> Void in
//            completion(result: self.getObjects(result["posts"] as! NSArray), success: true)
//            print(result)
//            }) { (error) -> Void in
//                completion(result: nil, success: false)
//        }
    }
    
    class func fetchQuestionsForm4(completion:(result : [PDOption]?, success : Bool) -> Void) {
        
        let responseString = "{\"Status\":\"success\",\"posts\":[\"Abnormal Bleeding\",\"Alcohol Abuse\",\"Anemia\",\"Angina Pectoris\",\"Anxiety\",\"Arthritis\",\"Artificial Heart Valve\",\"Artificial Joint\",\"Autism\",\"Auto Immune Conditions\",\"Asthma\",\"Back Problems\",\"Blood Transfusion\",\"Cancer\",\"Chemotherapy\",\"Congenital Heart Defect\",\"Diabetes\",\"Difficulty Breathing\",\"Drug Abuse\",\"Epilepsy\",\"Fainting Spells\",\"Frequent Headaches\",\"Frequent Fainting\",\"Glaucoma\",\"HIV+ AIDS\",\"Heart Attack\",\"Heart Murmur\",\"Heart Surgery\",\"Hemophilia\",\"Hepatitis A\",\" Hepatitis B\",\"Hepatitis C\",\"High Blood Pressure\",\"Joint Replacement\",\"Kidney Problems\",\"Liver Problems\",\"Low Blood Pressure\",\"Lupus\",\"Mitral Valve Prolapse\",\"Pace Maker\",\"Psychiatric Problems\",\"Radiation Therapy\",\"Respiratory Disease\",\"Rheumatic Fever\",\"Seizures\",\"Sexually Transmitted Disease\",\"Shingles\",\"Sickle Cell Disease\",\"Sinus Problems \",\"Skin Rash\",\"Stroke\",\"Thyroid Problems\",\"Tuberculosis\",\"Ulcers\"]}"
        
        let jsonObject = PDQuestion.getJSONObject(responseString)
        completion(result: self.getObjects(jsonObject!["posts"] as! NSArray), success: true)
        
//        ServiceManager.fetchDataFromService("disease_api.php", parameters: nil, success: { (result) -> Void in
//            completion(result: self.getObjects(result["posts"] as! NSArray), success: true)
//            print(result)
//            }) { (error) -> Void in
//                completion(result: nil, success: false)
//        }
    }
    
    class func fetchQuestionsToothExtractionForm1(completion:(result : [PDOption]?, success : Bool) -> Void) {
        let responseString = "{\"Status\":\"success\",\"posts\":[\"Pain\",\"Infection\",\"Decay\",\"Gum Disease\",\"Broken tooth\",\"Non Restorable\"]}"
        
        let jsonObject = PDQuestion.getJSONObject(responseString)
        completion(result: self.getObjects(jsonObject!["posts"] as! NSArray), success: true)
    }
    
    class func fetchQuestionsToothExtractionForm2(completion:(result : [PDOption]?, success : Bool) -> Void) {
        let responseString = "{\"Status\":\"success\",\"posts\":[\"No Treatment\",\"Root Canal Therapy\",\"Filling\",\"Crowns\",\"Gum Treatment\"]}"
        
        let jsonObject = PDQuestion.getJSONObject(responseString)
        completion(result: self.getObjects(jsonObject!["posts"] as! NSArray), success: true)
    }
}
