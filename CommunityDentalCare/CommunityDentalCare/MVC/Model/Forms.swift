//
//  Forms.swift
//  Advanced Aesthetic Dentistry
//
//  Created by samadsyed on 2/18/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit



let kFormsCompletedNotification = "kAllFormsCompletedSuccessfully"

let kVisitorCheckInForm = "VISITOR CHECK IN FORM"
let kNewPatientSignInForm = "NEW PATIENT SIGN-IN FORM"
let kMedicalHistoryForm = "MEDICAL HISTORY FORM"
let kDentalHistory = "DENTAL HISTORY FORM"
let kChildMedicalHistory = "CHILD MEDICAL HISTORY FORM"
let kChildDentalHistory = "CHILD DENTAL HISTORY FORM"

let kInsuranceCard = "SCAN INSURANCE CARD"
let kDrivingLicense = "SCAN DRIVING LICENSE"
let kFeedBack = "CUSTOMER REVIEW FORM"

let kConsentForms = "CONSENT FORMS"
let kToothRemoval = "INFORMED CONSENT TOOTH REMOVAL "
let kNoticeOfPrivacy = "NOTICE OF PRIVACY PRACTICES"
let kInformedConsent = "BEHAVIOR MANAGEMENT INFORMED CONSENT"
let kOfficePolicies = "OFFICE POLICIES"
let kQuestionaire = "QUESTIONAIRE FORM"
let toothNumberRequired = [kToothRemoval]
let kCommonDateFormat = "MMM dd, yyyy"

//let bloodPressureRequired = [KStatement]

extension String {
    var fileName : String {
        return self.stringByReplacingOccurrencesOfString(" - ", withString: "_").stringByReplacingOccurrencesOfString(" ", withString: "_").stringByReplacingOccurrencesOfString("/", withString: "_OR_")
    }
}

class Forms: NSObject {
    
    var formTitle : String!
    var subForms : [Forms]!
    var isSelected : Bool!
    var toothNumbers : String!
    var isToothNumberRequired : Bool!
    var isBloodPressureRequired : Bool!
    var index : Int!
    
    

    init(formDetails : NSDictionary) {
        super.init()
        self.isSelected = false
    }
    
    override init() {
        super.init()
    }
    
    class func getAllForms (completion :(isConnectionfailed: Bool, forms : [Forms]?) -> Void) {
        let isConnected = Reachability.isConnectedToNetwork()
        let forms = [kVisitorCheckInForm,kNewPatientSignInForm,kMedicalHistoryForm,kDentalHistory,kChildMedicalHistory,kChildDentalHistory,kInsuranceCard, kDrivingLicense, kFeedBack]
        let formObj = getFormObjects(forms, isSubForm: false)
        completion(isConnectionfailed: isConnected ? false : true, forms : formObj)
    }

    private class func getFormObjects (forms : [String], isSubForm : Bool) -> [Forms] {
        var formList : [Forms]! = [Forms]()
        for (idx, form) in forms.enumerate() {
            let formObj = Forms()
            formObj.isSelected = false
            formObj.index = isSubForm ? idx + 8 : idx
            formObj.formTitle = form
            formObj.isToothNumberRequired = toothNumberRequired.contains(form)

            
            if formObj.formTitle == kConsentForms {
                formObj.subForms = getFormObjects([kNoticeOfPrivacy,kInformedConsent], isSubForm:  true)
            }
//            if formObj.formTitle == kFeedBack {
//                
//                formObj.index = formList.count + formList[9].subForms.count + 1
//            }
            formList.append(formObj)
        }
        return formList
    }
    
}
