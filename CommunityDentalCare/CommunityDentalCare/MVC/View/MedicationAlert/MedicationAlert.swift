//
//  MedicationAlert.swift
//  CommunityDentalCare
//
//  Created by Bala Murugan on 9/27/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class MedicationAlert: UIView {
    
    @IBOutlet var textviewOther : UITextView!
    
    var arrayButtonTags : [Int] = [Int]()

    var completion:(([Int],String)->Void)?
    var errorAlert : ((String) -> Void)?

    static var sharedInstance : MedicationAlert {
        let instance :  MedicationAlert = NSBundle.mainBundle().loadNibNamed("MedicationAlert", owner: nil, options: nil)!.first as! MedicationAlert
        return instance
        
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
    
    func showPopUp(inview : UIView, completion : (arrayTags : [Int] , other : String) -> Void , showAlert : (alertMessage : String)-> Void)  {
        self.completion = completion
        self.errorAlert = showAlert
        textviewOther.text = ""
        arrayButtonTags.removeAll()
        self.frame = screenSize
        
        inview.addSubview(self)
        inview.bringSubviewToFront(self)
        self.subviews[0].transform = CGAffineTransformMakeScale(0.1, 0.1)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.subviews[0].transform = CGAffineTransformIdentity
        UIView.commitAnimations()
    }
    
    @IBAction func medicationsButtonAction (withSender sender : UIButton){
        sender.selected = !sender.selected
        if arrayButtonTags.contains(sender.tag){
            arrayButtonTags.removeAtIndex(arrayButtonTags.indexOf(sender.tag)!)
        }else{
            arrayButtonTags.append(sender.tag)
        }
        
        if sender.tag == 5 && sender.selected == true{
            textviewOther.userInteractionEnabled = true
            textviewOther.alpha = 1.0
        }else if sender.tag == 5 && sender.selected == false{
            textviewOther.userInteractionEnabled = false
            textviewOther.alpha = 0.5
        }
    }


    @IBAction func okButtonPressed (withSender sender : UIButton){
        if arrayButtonTags.contains(5) && textviewOther.isEmpty {
            self.errorAlert!("PLEASE SPECIFY THE OTHER MEDICATION")
        } else{
            self.completion!(arrayButtonTags,textviewOther.isEmpty ? "N/A" : textviewOther.text!)
            self.removeFromSuperview()
        }
    }

}
